package it.com.atlassian.plugin.refimpl;

import it.com.atlassian.refapp.RefappTestURLs;
import it.com.atlassian.refapp.rest.RestUtils;
import org.codehaus.jackson.JsonNode;

import static com.atlassian.healthcheck.checks.HealthCheckConstants.HEALTH_CHECK_DETAILS_JSON;
import static com.atlassian.healthcheck.checks.HealthCheckConstants.HEALTH_CHECK_STATUS_FIELD;

class HealthCheckTestUtils {

    static JsonNode getHealthChecksStatusArray() {
        return RestUtils.getJsonResponse(RestUtils.newResource(RefappTestURLs.BASEURL + HEALTH_CHECK_DETAILS_JSON))
                .get(HEALTH_CHECK_STATUS_FIELD);
    }
}
