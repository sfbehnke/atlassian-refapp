package it.com.atlassian.plugin.refimpl;

import com.atlassian.healthcheck.checks.HealthCheckConstants;
import com.atlassian.healthcheck.checks.plugin.PluginHealthCheckConstants;
import org.codehaus.jackson.JsonNode;
import org.junit.Test;

import java.util.List;

import static com.atlassian.healthcheck.checks.HealthCheckConstants.HEALTH_CHECK_IS_HEALTHY_FIELD;
import static com.atlassian.healthcheck.checks.HealthCheckConstants.HEALTH_CHECK_NAME_FIELD;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;

/**
 * Check all the healthchecks from atlassian-healthcheck plugin pass.
 * <p>
 * See similar com.atlassian.confluence.HealthCheckAcceptanceTest in Confluence.
 *
 * @see TestPluginStartup for tests against some other specific healthchecks
 */
public class TestHealthChecks {

    @Test
    public void testExpectedHealthchecksPresent() {
        JsonNode statusArray = HealthCheckTestUtils.getHealthChecksStatusArray();

        List<String> checkNames = newArrayList();
        for (JsonNode status : statusArray) {
            checkNames.add(status.get(HEALTH_CHECK_NAME_FIELD).asText());
        }

        assertThat("Some of the expected healthchecks were not present."
                    + " Check that plugins containing them have initialised ok,"
                    + " or whether their <health-check>'s name field has been changed",
                checkNames,
                containsInAnyOrder(
                        HealthCheckConstants.HOST_APPLICATION_STATUS_HEALTH_CHECK,
                        PluginHealthCheckConstants.DISABLED_PLUGINS_HEALTHCHECK,
                        PluginHealthCheckConstants.DISABLED_PLUGIN_MODULES_HEALTHCHECK,
                        PluginHealthCheckConstants.DISABLED_BY_DEFAULT_PLUGINS_HEALTHCHECK,
                        PluginHealthCheckConstants.DISABLED_BY_DEFAULT_PLUGIN_MODULES_HEALTHCHECK));
    }

    @Test
    public void testAllHealthChecksPass() throws Exception {
        JsonNode statusArray = HealthCheckTestUtils.getHealthChecksStatusArray();

        List<JsonNode> errors = newArrayList();
        for (JsonNode status : statusArray) {
            if (!status.get(HEALTH_CHECK_IS_HEALTHY_FIELD).asBoolean()) {
                errors.add(status);
            }
        }

        assertThat("All healthchecks should pass, but these ones failed: ", errors, empty());
    }

}
