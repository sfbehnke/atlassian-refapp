package it.com.atlassian.plugin.refimpl;

import com.atlassian.webdriver.refapp.page.RefappAdminHomePage;
import com.atlassian.webdriver.refapp.page.RefappCharlieAdminPage;
import com.atlassian.webdriver.refapp.page.RefappHomePage;
import com.atlassian.webdriver.refapp.page.RefappLoginPage;
import com.atlassian.webdriver.refapp.page.RefappWebSudoPage;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TestWebSudo extends AbstractRefappTestCase {

    private static final String TEST_USER = "admin";
    private static final String TEST_PASS = "admin";

    @After
    public void tearDown() throws Exception {
        PRODUCT.gotoHomePage().getHeader().logout(RefappHomePage.class);
    }

    @Test
    public void testWebSudoByPass() {
        loginAs(TEST_USER, TEST_PASS, true);
        RefappCharlieAdminPage charlieAdminPage = PRODUCT.visit(RefappCharlieAdminPage.class);
        assertTrue(charlieAdminPage.getHeader().getWebSudoBanner().getMessage().contains("You have temporary access to administrative functions."));
    }

    @Test
    public void testWebSudoRequired() {
        loginAs(TEST_USER, TEST_PASS);

        PRODUCT.visit(RefappCharlieAdminPage.class);
        RefappWebSudoPage webSudoPage = PRODUCT.getPageBinder().bind(RefappWebSudoPage.class);
        RefappCharlieAdminPage charlieAdminPage = webSudoPage.confirm(TEST_PASS, RefappCharlieAdminPage.class);
        assertTrue(charlieAdminPage.getHeader().getWebSudoBanner().getMessage().contains("You have temporary access to administrative functions."));
    }

    @Test
    public void testWebSudoRequiredWrongPassword() {
        loginAs(TEST_USER, TEST_PASS);

        PRODUCT.visit(RefappCharlieAdminPage.class);
        RefappWebSudoPage webSudoPage = PRODUCT.getPageBinder().bind(RefappWebSudoPage.class);
        webSudoPage = webSudoPage.confirm("", RefappWebSudoPage.class);
        webSudoPage = webSudoPage.confirm("blah", RefappWebSudoPage.class);
        assertTrue(webSudoPage.isRequestAccessMessagePresent());
    }

    @Test
    public void testWebSudoStateIsShown() {
        loginAs(TEST_USER, TEST_PASS, true);
        RefappCharlieAdminPage charlieAdminPage = PRODUCT.visit(RefappCharlieAdminPage.class);
        assertTrue(charlieAdminPage.getHeader().getWebSudoBanner().getMessage().contains("You have temporary access to administrative functions."));
    }

    @Test
    public void testWebSudoPrivilegesCanBeDropped() {
        loginAs(TEST_USER, TEST_PASS, true);
        RefappCharlieAdminPage charlieAdminPage = PRODUCT.visit(RefappCharlieAdminPage.class);
        assertTrue(charlieAdminPage.getHeader().getWebSudoBanner().getMessage().contains("You have temporary access to administrative functions."));

        RefappHomePage homePage = charlieAdminPage.getHeader().getWebSudoBanner().dropWebSudo(RefappHomePage.class);
        assertNull(homePage.getHeader().getWebSudoBanner().getMessage());
    }

    private RefappAdminHomePage loginAs(final String user, final String password) {
        RefappLoginPage loginPage = PRODUCT.gotoLoginPage();
        return loginPage.login(user, password, false, RefappAdminHomePage.class);
    }

    private RefappAdminHomePage loginAs(final String user, final String password, boolean bypassWebsudo) {
        RefappLoginPage loginPage = PRODUCT.gotoLoginPage();
        return loginPage.login(user, password, bypassWebsudo, RefappAdminHomePage.class);
    }
}