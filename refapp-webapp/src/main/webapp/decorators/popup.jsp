<%@ page import="com.atlassian.webresource.api.assembler.WebResourceAssembler" %>
<%@ page import="com.atlassian.webresource.api.UrlMode" %>
<%@ page import="com.atlassian.plugin.refimpl.ContainerManager" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>

<html>
    <head>
        <title><decorator:title default="Welcome!" /></title>
        <decorator:head />
        <%
            WebResourceAssembler assembler = ContainerManager.getInstance().getPageBuilderService().assembler();
            assembler.assembled().drainIncludedResources().writeHtmlTags(out, UrlMode.AUTO);
        %>
    </head>

    <body>
        <h2>
            Atlassian Plugins Reference Implementation - Popup Decorator
        </h2>
        <decorator:body />
        <hr>
        <div style="text-align:center">
            Atlassian Plugins -
            <a href="https://ecosystem.atlassian.net/browse/PLUG">Issues</a> |
            <a href="https://bamboo.extranet.atlassian.com/browse/PLUG">Builds</a> |
            <a href="https://developer.atlassian.com/display/PLUGINFRAMEWORK">Documentation</a>
        </div>
    </body>
</html>
