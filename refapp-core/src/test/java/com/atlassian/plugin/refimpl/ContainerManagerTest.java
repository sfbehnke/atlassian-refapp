package com.atlassian.plugin.refimpl;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.events.PluginFrameworkDelayedEvent;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.test.PluginJarBuilder;
import com.atlassian.tenancy.api.TenantContext;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.servlet.ServletContext;
import java.io.File;

import static com.atlassian.plugin.refimpl.tenant.RefappTenancyCondition.getAtlassianTenancyEnabledProperty;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.apache.commons.io.FileUtils.forceMkdir;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Tests for the {@link ContainerManager}.
 *
 * When running this test you will see ERROR logs about org.springframework.web not starting because it can't resolve the servlet
 * package. This is because this component is provided at runtime by the servlet container, that is,
 * Tomcat. I think the right fix here is to allow the framework bundles to be configured for this test, since we don't need the
 * full set of framework bundles in this case. Even better, we could break up ContainerManager into more testable pieces, as it
 * is quite a monolith.
 */
@RunWith(org.mockito.runners.MockitoJUnitRunner.class)
public class ContainerManagerTest {
    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties(getAtlassianTenancyEnabledProperty());

    @Mock
    private ServletContext servletContext;

    private File webappDirectory;
    private File frameworkBundles;

    public static class Helper {
        static final String PLUGIN_KEY = "test.helper";

        private final TenantContext tenantContext;

        private Boolean tenantAvailableAfterEarlyStartup;

        public Helper(final EventPublisher eventPublisher, final TenantContext tenantContext) {
            this.tenantContext = tenantContext;
            eventPublisher.register(this);
        }

        public Boolean getTenantAvailableAfterEarlyStartup() {
            return tenantAvailableAfterEarlyStartup;
        }

        @PluginEventListener
        public void onEarlyStartupComplete(PluginFrameworkDelayedEvent pluginFrameworkDelayedEvent) {
            tenantAvailableAfterEarlyStartup = (null != tenantContext.getCurrentTenant());
        }
    }

    @Before
    public void setUp() throws Exception {
        webappDirectory = new File("target/" + ContainerManagerTest.class.getName());
        frameworkBundles = new File("target/framework-bundles");
        System.setProperty("framework.bundles", "target/framework-bundles");
        forceMkdir(webappDirectory);
        when(servletContext.getRealPath(any(String.class))).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocationOnMock) throws Throwable {
                String path = (String) invocationOnMock.getArguments()[0];
                // Paths that aren't absolute won't work with Tomcat 8, so let's stop them now
                assertThat(path, startsWith("/"));
                return new File(webappDirectory, path).getPath();
            }
        });
        when(servletContext.getMajorVersion()).thenReturn(3);
        when(servletContext.getMinorVersion()).thenReturn(1);
        final File pluginsDirectory = new File(webappDirectory, "/WEB-INF/plugins");
        forceMkdir(pluginsDirectory);
        new PluginJarBuilder("testHelper")
                .addFormattedResource("atlassian-plugin.xml",
                        "<atlassian-plugin name='TestHelper' key='" + Helper.PLUGIN_KEY + "' pluginsVersion='2'>",
                        "    <plugin-info>",
                        "        <version>1.0</version>",
                        "    </plugin-info>",
                        "    <component key='helper' class='" + Helper.class.getName() + "'/>",
                        "</atlassian-plugin>")
                .build(pluginsDirectory);
    }

    @After
    public void tearDown() throws Exception {
        deleteQuietly(webappDirectory);
    }

    @Test
    public void tenantAvailableAfterEarlyStartupWhenTenancyNotEnabled() {
        final ContainerManager containerManager = new ContainerManager(servletContext);
        try {
            final Helper helper = getHelper(containerManager);
            assertThat(helper.getTenantAvailableAfterEarlyStartup(), is(Boolean.TRUE));
        } finally {
            containerManager.shutdown();
        }
    }

    @Test
    public void tenantNotAvailableAfterEarlyStatupWhenTenancyEnabled() {
        System.setProperty(getAtlassianTenancyEnabledProperty(), "true");
        final ContainerManager containerManager = new ContainerManager(servletContext);
        try {
            final Helper helper = getHelper(containerManager);
            assertThat(helper.getTenantAvailableAfterEarlyStartup(), is(Boolean.FALSE));
        } finally {
            containerManager.shutdown();
        }
    }

    private Helper getHelper(final ContainerManager containerManager) {
        final Plugin plugin = containerManager.getPluginAccessor().getPlugin(Helper.PLUGIN_KEY);
        assertThat(plugin, instanceOf(ContainerManagedPlugin.class));
        final Object helperBean = ((ContainerManagedPlugin) plugin).getContainerAccessor().getBean("helper");
        assertThat(helperBean, Matchers.instanceOf(Helper.class));
        return (Helper) helperBean;
    }
}
