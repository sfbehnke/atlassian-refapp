package com.atlassian.plugin.refimpl.cache;

import com.atlassian.vcache.VCacheFactory;
import com.atlassian.vcache.internal.RequestContext;
import com.atlassian.vcache.internal.test.EmptyVCacheSettingsDefaultsProvider;
import com.atlassian.vcache.internal.core.DefaultVCacheCreationHandler;
import com.atlassian.vcache.internal.core.ThreadLocalRequestContextSupplier;
import com.atlassian.vcache.internal.core.metrics.DefaultMetricsCollector;
import com.atlassian.vcache.internal.guava.GuavaServiceSettingsBuilder;
import com.atlassian.vcache.internal.guava.GuavaVCacheService;

import java.time.Duration;
import java.util.function.Supplier;

import static com.atlassian.vcache.ChangeRate.HIGH_CHANGE;

/**
 * I chose to follow org.springframework.beans.factory.FactoryBean interface even though we don't have spring here,
 * because we want to add Dependency Injection at some point (see REFAPPDEV-13) and it's likely to be Spring, thus when
 * we do add it we could just add 'implements' to this one do the happy dance
 */
public class VCacheFactoryBean {
    private static final String PRODUCT_IDENTIFIER = "refapp";

    private final Supplier<RequestContext> requestContextSupplier = ThreadLocalRequestContextSupplier.lenientSupplier(() -> "refapp-tenant");
    private final VCacheFactory vCacheFactory = new GuavaVCacheService(
            PRODUCT_IDENTIFIER,
            requestContextSupplier,
            new EmptyVCacheSettingsDefaultsProvider(),
            cacheCreationHandler(),
            new DefaultMetricsCollector(requestContextSupplier),
            new GuavaServiceSettingsBuilder().build(),
            context -> {}
    );

    public VCacheFactoryBean() {
    }

    public VCacheFactory getObject()
    {
        return vCacheFactory;
    }

    Class getObjectType() {
        return VCacheFactory.class;
    }

    boolean isSingleton() {
        return true;
    }

    private DefaultVCacheCreationHandler cacheCreationHandler()
    {
        return new DefaultVCacheCreationHandler(10_000, Duration.ofHours(1), 10_000, HIGH_CHANGE, HIGH_CHANGE);
    }
}
