package com.atlassian.plugin.refimpl.saldeps;

import com.atlassian.plugin.scope.ScopeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toMap;

public class CookieBasedScopeManager implements ScopeManager {

    private final static String COOKIE_PREFIX = "atlassian.scope.";

    private static final Logger log = LoggerFactory.getLogger(CookieBasedScopeManager.class);

    @Override
    public boolean isScopeActive(String scopeKey) {
        final HttpServletRequest request = ServletContextThreadLocal.getRequest();
        if (request != null) {
            final Cookie[] cookies = ServletContextThreadLocal.getRequest().getCookies();
            if (cookies != null) {
                final Map<String, String> map = stream(cookies).collect(toMap(Cookie::getName, Cookie::getValue));
                final String cookieValue = map.get(COOKIE_PREFIX + scopeKey);
                if (cookieValue != null) {
                    log.info("Detected scope cookie for '{}' key", scopeKey);
                    return Boolean.valueOf(cookieValue);
                }
            } else {
                log.info("No cookies were found in request (tumbleweed)");
            }
            //false if
            return false;
        }
        //true for all background jobs/threads for now
        return true;
    }
}