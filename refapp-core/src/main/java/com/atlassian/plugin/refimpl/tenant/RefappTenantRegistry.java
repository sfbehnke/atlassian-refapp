package com.atlassian.plugin.refimpl.tenant;

import com.atlassian.tenancy.api.Tenant;
import com.atlassian.tenancy.api.TenantAccessor;
import com.atlassian.tenancy.api.TenantContext;
import com.atlassian.tenancy.api.TenantUnavailableException;
import com.google.common.base.Preconditions;

import javax.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Dummy single tenant accessor - always there
 */
public class RefappTenantRegistry implements TenantAccessor, TenantContext {
    private RefappTenant tenant;

    @Override
    public Iterable<Tenant> getAvailableTenants() {
        if (tenant != null) {
            List<Tenant> list = new ArrayList<Tenant>();
            list.add(tenant);
            return list;
        } else {
            return new ArrayList<Tenant>();
        }
    }

    public List<RefappTenant> getRefappTenants() {
        List<RefappTenant> list = new ArrayList<RefappTenant>();
        if (tenant != null) {
            list.add(tenant);
        }
        return list;
    }


    public void setTenant(RefappTenant tenant) {
        this.tenant = tenant;
    }


    public boolean hasTenant() {
        return (tenant != null);
    }


    @Nullable
    @Override
    public Tenant getCurrentTenant() {
        return tenant;
    }


    @Override
    public <T> T asTenant(final Tenant tenant, final Callable<T> callback)
            throws TenantUnavailableException, InvocationTargetException {
        Preconditions.checkNotNull(tenant);
        Preconditions.checkNotNull(callback);

        if (tenant != this.tenant) {
            throw new TenantUnavailableException();
        }

        try {
            return callback.call();
        } catch (Exception e) {
            throw new InvocationTargetException(e);
        }
    }
}