package com.atlassian.plugin.refimpl.saldeps;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletContextThreadLocal {
    private static final ThreadLocal<HttpServletRequest> request = new ThreadLocal<HttpServletRequest>();
    private static final ThreadLocal<HttpServletResponse> response = new ThreadLocal<HttpServletResponse>();

    public static ServletContext getContext() {
        return getRequest().getSession().getServletContext();
    }

    public static HttpServletRequest getRequest() {
        return request.get();
    }

    /**
     * @param httpRequest the request
     * @deprecated since 2.16. This method is not longer a part of public API and
     * should not be used from outside of <code>com.atlassian.core.filters</code> package.
     * The visibility of this method will be changed to package private in the future;
     * no replacement provided.
     */
    public static void setRequest(HttpServletRequest httpRequest) {
        request.set(httpRequest);
    }

    /**
     * @param httpResponse the response
     * @deprecated since 2.18. This method is not longer a part of public API and
     * should not be used from outside of <code>com.atlassian.core.filters</code> package.
     * The visibility of this method will be changed to package private in the future;
     * no replacement provided.
     */
    public static void setResponse(HttpServletResponse httpResponse) {
        response.set(httpResponse);
    }

    public static HttpServletResponse getResponse() {
        return response.get();
    }
}
