package com.atlassian.plugin.refimpl.tenant;

/**
 * Helper class to test if tenancy is enabled, controlled by system property atlassian.tenancy.enabled
 *
 * @since v2.21.4
 */
public class RefappTenancyCondition {
    private static final String ATLASSIAN_TENANCY_ENABLED_PROPERTY = "atlassian.tenancy.enabled";

    public static String getAtlassianTenancyEnabledProperty() {
        return ATLASSIAN_TENANCY_ENABLED_PROPERTY;
    }

    public static boolean isEnabled() {
        return Boolean.getBoolean(ATLASSIAN_TENANCY_ENABLED_PROPERTY);
    }
}
