package com.atlassian.plugin.refimpl;

import com.atlassian.plugin.webresource.UrlMode;

import java.net.URI;

public class ParameterUtils {
    public static String getBaseUrl(UrlMode urlMode) {
        String port = System.getProperty("http.port", "5990");
        String baseUrl = System.getProperty("baseurl", "http://localhost:" + port + "/atlassian-refapp");
        if (urlMode == UrlMode.ABSOLUTE) {
            return baseUrl;
        }
        return URI.create(baseUrl).getPath();
    }
}
