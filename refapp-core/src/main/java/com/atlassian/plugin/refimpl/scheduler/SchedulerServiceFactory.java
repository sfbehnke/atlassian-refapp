package com.atlassian.plugin.refimpl.scheduler;

import com.atlassian.plugin.refimpl.tenant.RefappTenantRegistry;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.core.LifecycleAwareSchedulerService;
import com.atlassian.scheduler.core.impl.MemoryRunDetailsDao;
import com.atlassian.scheduler.quartz1.Quartz1DefaultSettingsFactory;
import com.atlassian.scheduler.quartz1.Quartz1SchedulerService;
import com.atlassian.scheduler.quartz1.spi.Quartz1SchedulerConfiguration;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Properties;
import java.util.TimeZone;

public final class SchedulerServiceFactory {
    public static LifecycleAwareSchedulerService inMemoryWithDefaultSettings() {
        LifecycleAwareSchedulerService schedulerService;
        try {
            schedulerService = new Quartz1SchedulerService(
                    new MemoryRunDetailsDao(),
                    new Quartz1SchedulerConfiguration() {
                        @Nonnull
                        @Override
                        public Properties getLocalSettings() {
                            return Quartz1DefaultSettingsFactory.getDefaultLocalSettings();
                        }

                        @Nonnull
                        @Override
                        public Properties getClusteredSettings() {
                            return Quartz1DefaultSettingsFactory.getDefaultLocalSettings();
                        }

                        @Nullable
                        @Override
                        public TimeZone getDefaultTimeZone() {
                            return TimeZone.getDefault();
                        }
                    },
                    new RefappTenantRegistry()
            );
        } catch (SchedulerServiceException e) {
            schedulerService = null;
        }

        return schedulerService;
    }

    private SchedulerServiceFactory() {
    }
}
