package com.atlassian.plugin.refimpl.webresource;

import com.atlassian.plugin.refimpl.CurrentHttpRequest;
import com.atlassian.plugin.webresource.DefaultResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.google.common.collect.ImmutableList;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Config class for batching that detects if the refapp should batch for all browsers, just IE or none at all. Resources
 * that need to be super-batched are hardcoded within the class.
 */

public class RefAppResourceBatchingConfiguration extends DefaultResourceBatchingConfiguration
        implements ResourceBatchingConfiguration {

    private static String SUPER_BATCH_FOR = System.getProperty("super.batch.for");
    private static final boolean DEV_MODE = Boolean.getBoolean("atlassian.dev.mode");

    private static final List<String> resources = ImmutableList.of(
            "com.atlassian.refapp.amd:amd",
            "com.atlassian.auiplugin:ajs"
    );

    @Override
    public boolean isSuperBatchingEnabled() {
        return forceBatchingInThisRequest();
    }

    @Override
    public List<String> getSuperBatchModuleCompleteKeys() {
        return resources;
    }

    @Override
    public boolean isContextBatchingEnabled() {
        return !DEV_MODE;
    }

    @Override
    public boolean isPluginWebResourceBatchingEnabled() {
        return !DEV_MODE || forceBatchingInThisRequest();
    }

    private boolean forceBatchingInThisRequest() {
        // Only force batching if SUPER_BATCH_FOR variable is set, always return true if SUPER_BATCH_FOR is set to 'all'
        if ("all".equalsIgnoreCase(SUPER_BATCH_FOR)) {
            return true;
        }
        //if SUPER_BATCH_FOR is set to 'ie' return true only when the user agent of the request is IE.
        else if ("ie".equalsIgnoreCase(SUPER_BATCH_FOR)) {
            HttpServletRequest httpRequest = CurrentHttpRequest.getRequest();
            if (httpRequest == null) {
                return false;
            }
            //only batch if the request came from IE (very simple implementation)'

            String userAgent = httpRequest.getHeader("USER-AGENT");

            if (userAgent != null) {
                return userAgent.contains("Trident");
            }
        }

        //return false (turn super batch off) if variable isn't set
        return false;
    }
}
