package com.atlassian.plugin.refimpl;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.beehive.SimpleClusterLockService;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.healthcheck.spi.HealthCheckWhitelist;
import com.atlassian.healthcheck.spi.impl.ClasspathFileHealthCheckWhitelist;
import com.atlassian.plugin.Application;
import com.atlassian.plugin.DefaultModuleDescriptorFactory;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.SplitStartupPluginSystemLifecycle;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.eventlistener.descriptors.EventListenerModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.hostcontainer.SimpleConstructorHostContainer;
import com.atlassian.plugin.main.AtlassianPlugins;
import com.atlassian.plugin.main.PluginsConfiguration;
import com.atlassian.plugin.main.PluginsConfigurationBuilder;
import com.atlassian.plugin.metadata.DefaultPluginMetadataManager;
import com.atlassian.plugin.metadata.PluginMetadataManager;
import com.atlassian.plugin.module.ClassPrefixModuleFactory;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.module.PrefixDelegatingModuleFactory;
import com.atlassian.plugin.module.PrefixModuleFactory;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.impl.DefaultPackageScannerConfiguration;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.hostcomponents.ComponentRegistrar;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import com.atlassian.plugin.osgi.module.BeanPrefixModuleFactory;
import com.atlassian.plugin.refimpl.cache.VCacheFactoryBean;
import com.atlassian.plugin.refimpl.db.ConnectionProviderImpl;
import com.atlassian.plugin.refimpl.saldeps.CookieBasedScopeManager;
import com.atlassian.plugin.refimpl.saldeps.CoreRefimplI18nResolver;
import com.atlassian.plugin.refimpl.saldeps.CoreRefimplLocaleResolver;
import com.atlassian.plugin.refimpl.servlet.SimpleContentTypeResolver;
import com.atlassian.plugin.refimpl.servlet.SimpleServletContextFactory;
import com.atlassian.plugin.refimpl.tenant.RefappLandlordRequests;
import com.atlassian.plugin.refimpl.tenant.RefappTenancyManager;
import com.atlassian.plugin.refimpl.tenant.RefappTenantRegistry;
import com.atlassian.plugin.refimpl.webresource.RefAppResourceBatchingConfiguration;
import com.atlassian.plugin.refimpl.webresource.SimpleWebResourceIntegration;
import com.atlassian.plugin.schema.descriptor.DescribedModuleDescriptorFactory;
import com.atlassian.plugin.schema.impl.DefaultDescribedModuleDescriptorFactory;
import com.atlassian.plugin.servlet.DefaultServletModuleManager;
import com.atlassian.plugin.servlet.DownloadStrategy;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.descriptors.ServletContextListenerModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletContextParamModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletFilterModuleDescriptor;
import com.atlassian.plugin.servlet.descriptors.ServletModuleDescriptor;
import com.atlassian.plugin.util.Assertions;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.PluginResourceLocatorImpl;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugin.webresource.WebResourceManagerImpl;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.WebResourceUrlProviderImpl;
import com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.prebake.PrebakeWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.servlet.PluginResourceDownload;
import com.atlassian.plugin.webresource.transformer.StaticTransformersSupplier;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.plugins.landlord.spi.LandlordRequests;
import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.tenancy.api.TenantAccessor;
import com.atlassian.tenancy.api.TenantContext;
import com.atlassian.vcache.VCacheFactory;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * A simple class that behaves like Spring's ContainerManager class.
 */
public class ContainerManager {
    /**
     * Object accessed via the servletContext attributes
     */
    public static final String TENANTACCESSOR = "tenantAccessor";

    /**
     * System property key for overriding package export versions.
     */
    private static final String PACKAGE_VERSION_EXPORT_OVERRIDES = "refapp.packageExport.overrides";

    /**
     * The directory containing all bundled plugins.
     */
    private static final String BUNDLED_PLUGINS_DIR = "/WEB-INF/atlassian-bundled-plugins";

    private final ServletModuleManager servletModuleManager;
    private final SimpleWebResourceIntegration webResourceIntegration;
    private final OsgiContainerManager osgiContainerManager;
    private final PluginAccessor pluginAccessor;
    private final HostComponentProvider hostComponentProvider;
    private final DefaultModuleDescriptorFactory moduleDescriptorFactory;
    private final Map<Class<?>, Object> publicContainer;
    private final AtlassianPlugins plugins;
    private final SplitStartupPluginSystemLifecycle pluginLifecycle;
    private final RefappTenantRegistry tenantAccessor;
    private final RefappLandlordRequests landlordImpl;
    private final RefappTenancyManager tenancyManager;
    private final ClusterLockService clusterLockService;
    private final ConnectionProviderImpl connectionProvider;

    private final PluginPhaseTriggers triggers = new PluginPhaseTriggers();

    private final HostContainer hostContainer;
    private static ContainerManager instance;
    private final List<DownloadStrategy> downloadStrategies;
    private final DefaultPageBuilderService pageBuilderService;

    public ContainerManager(final ServletContext servletContext) {
        instance = this;
        try {
            Class factoryFinder = Class.forName("javax.xml.transform.FactoryFinder");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        // Delegating host container since the real one requires the created object map, which won't be available until later
        final HostContainer delegatingHostContainer = new HostContainer() {
            public <T> T create(final Class<T> moduleClass) throws IllegalArgumentException {
                return hostContainer.create(moduleClass);
            }
        };

        try {
            connectionProvider = new ConnectionProviderImpl();
        } catch (Exception e) {
            // we don't want to start the rest of the container without a valid database available
            throw new RuntimeException(e);
        }

        moduleDescriptorFactory = new DefaultDescribedModuleDescriptorFactory(delegatingHostContainer);

        moduleDescriptorFactory.addModuleDescriptor("servlet", ServletModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("servlet-filter", ServletFilterModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("servlet-context-param", ServletContextParamModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("servlet-context-listener", ServletContextListenerModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("web-resource", WebResourceModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("web-resource-transformer", WebResourceTransformerModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("url-reading-web-resource-transformer", UrlReadingWebResourceTransformerModuleDescriptor.class);
        moduleDescriptorFactory.addModuleDescriptor("listener", EventListenerModuleDescriptor.class);

        final DefaultPackageScannerConfiguration scannerConfig = new DefaultPackageScannerConfiguration(determineVersion());
        scannerConfig.setServletContext(servletContext);

        final List<String> packageIncludes = new ArrayList<String>(scannerConfig.getPackageIncludes());
        packageIncludes.add("org.bouncycastle*");
        packageIncludes.add("org.dom4j*");
        packageIncludes.add("javax.servlet*");
        packageIncludes.add("com.google.common.*");
        packageIncludes.add("com.atlassian.sal*");
        packageIncludes.add("com.opensymphony.module.sitemesh*");
        packageIncludes.add("org.apache.commons.logging*");
        scannerConfig.setPackageIncludes(packageIncludes);

        final List<String> packageExcludes = new ArrayList<String>(scannerConfig.getPackageExcludes());
        packageExcludes.add("com.atlassian.healthcheck.spi.impl");
        scannerConfig.setPackageExcludes(packageExcludes);

        final Map<String, String> packageVersions = Maps.<String, String>newHashMap();
        packageVersions.put("com.google.common.*", SystemExportVersionUtils.getGoogleGuavaVersion());
        // org.w3c.dom.html comes from both xercesImpl and xml-apis. To avoid package scanner non-deterministically guessing a
        // version, we force the version here. Following JIRA's lead, we'll version the whole package to nothing.
        packageVersions.put("org.w3c.*", "");
        // javax.annotation is present both in WEB-INF/lib/jsr305-1.3.9.jar and tomcat's lib/annotations-api.jar, and there's also
        // a bit in ${JAVA_HOME}/jre/lib/rt.jar. As above, we specify a blank version. In fact this version is changed to 1.0.0 by
        // the plugin system *after* scanning, but we still wish to avoid the warning. We do just javax.annotation, and no
        // subpackages, because there's no clash amongst the subpackages.
        packageVersions.put("javax.annotation", "");
        final String packageVersionExportOverrides = System.getProperty(PACKAGE_VERSION_EXPORT_OVERRIDES);
        if (packageVersionExportOverrides != null) {
            packageVersions.putAll(ConfigParser.parseMap(packageVersionExportOverrides));
        }
        scannerConfig.setPackageVersions(packageVersions);

        hostComponentProvider = new SimpleHostComponentProvider();

        File osgiCache = findAndCreateDirectory(servletContext, "osgi.cache", "/WEB-INF/osgi-cache");
        File frameworkBundlesDir = findAndCreateDirectory(servletContext, "framework.bundles", "/WEB-INF/framework-bundles");
        final URL bundledPluginDir = getBundledPluginDir(servletContext);
        // When bundledPluginUrl is a directory, bundledPluginCacheDirectory is not used at all.
        // But we have to keep it to make the validation happy.
        final File legacyBundledPluginCacheDirectory = FileUtils.toFile(bundledPluginDir);

        final CookieBasedScopeManager scopeManager = new CookieBasedScopeManager();

        final PluginsConfiguration config = new PluginsConfigurationBuilder()
                .useLegacyDynamicPluginDeployer(true)
                .frameworkBundleDirectory(frameworkBundlesDir)
                .bundledPluginUrl(bundledPluginDir)
                .bundledPluginCacheDirectory(legacyBundledPluginCacheDirectory)
                .pluginDirectory(makeSureDirectoryExists(servletContext, "/WEB-INF/plugins"))
                .moduleDescriptorFactory(moduleDescriptorFactory)
                .packageScannerConfiguration(scannerConfig)
                .hostComponentProvider(hostComponentProvider)
                .osgiPersistentCache(osgiCache)
                .pluginStateStore(new DefaultPluginPersistentStateStore(osgiCache))
                .application(refapp())
                .scopeManager(scopeManager)
                .build();

        PrefixDelegatingModuleFactory moduleFactory = new PrefixDelegatingModuleFactory(ImmutableSet.<PrefixModuleFactory>of(new BeanPrefixModuleFactory()));
        plugins = new AtlassianPlugins(config);

        final PluginEventManager pluginEventManager = plugins.getPluginEventManager();
        osgiContainerManager = plugins.getOsgiContainerManager();

        servletModuleManager = new DefaultServletModuleManager(servletContext, pluginEventManager, scopeManager);
        pluginAccessor = plugins.getPluginAccessor();

        pluginLifecycle = plugins.getPluginSystemLifecycle();

        webResourceIntegration = new SimpleWebResourceIntegration(
                servletContext,
                new CoreRefimplLocaleResolver(),
                new CoreRefimplI18nResolver(pluginAccessor, pluginEventManager),
                pluginEventManager,
                determineVersion()
        );
        WebResourceUrlProvider webResourceUrlProvider = new WebResourceUrlProviderImpl(webResourceIntegration);

        //use the refapp implementation to configure super batch
        final RefAppResourceBatchingConfiguration refappBatchconfig = new RefAppResourceBatchingConfiguration();

        final ServletContextFactory simpleServletContextFactory = new SimpleServletContextFactory(servletContext);
        final PluginResourceLocator pluginResourceLocator = new PluginResourceLocatorImpl(webResourceIntegration, simpleServletContextFactory, webResourceUrlProvider, refappBatchconfig, pluginEventManager);
        final PluginResourceDownload pluginDownloadStrategy = new PluginResourceDownload(pluginResourceLocator, new SimpleContentTypeResolver(), "UTF-8");

        PrebakeWebResourceAssemblerFactory prebakeWebResourceAssemblerFactory = new DefaultWebResourceAssemblerFactory(pluginResourceLocator);

        pageBuilderService = new DefaultPageBuilderService(webResourceIntegration, prebakeWebResourceAssemblerFactory);

        EventPublisher eventPublisher = plugins.getEventPublisher();

        // Instant-On: landlord and tenant components
        WebResourceManager webResourceManager = new WebResourceManagerImpl(pluginResourceLocator, webResourceIntegration, webResourceUrlProvider, refappBatchconfig);
        tenantAccessor = new RefappTenantRegistry();
        landlordImpl = new RefappLandlordRequests(triggers.getTenantTrigger(), tenantAccessor);
        tenancyManager = new RefappTenancyManager(landlordImpl);
        servletContext.setAttribute(TENANTACCESSOR, tenantAccessor);// Tenant filter needs access :-(

        // Cluster lock service, provided by the beehive module
        clusterLockService = new SimpleClusterLockService();

        //VCache Factory instantiation
        VCacheFactory vCacheFactory = new VCacheFactoryBean().getObject();

        publicContainer = new HashMap<Class<?>, Object>();
        publicContainer.put(VCacheFactory.class, vCacheFactory);
        publicContainer.put(PluginController.class, plugins.getPluginController());

        publicContainer.put(ClusterLockService.class, clusterLockService);
        publicContainer.put(SplitStartupPluginSystemLifecycle.class, pluginLifecycle);
        publicContainer.put(EventPublisher.class, eventPublisher);
        publicContainer.put(TenantAccessor.class, tenantAccessor);
        publicContainer.put(TenantContext.class, tenantAccessor);
        publicContainer.put(LandlordRequests.class, landlordImpl);
        publicContainer.put(ServletModuleManager.class, servletModuleManager);
        publicContainer.put(WebResourceManager.class, webResourceManager);
        publicContainer.put(WebResourceUrlProvider.class, webResourceUrlProvider);
        publicContainer.put(ResourceBatchingConfiguration.class, refappBatchconfig);
        publicContainer.put(WebResourceIntegration.class, webResourceIntegration);
        publicContainer.put(WebResourceAssemblerFactory.class, prebakeWebResourceAssemblerFactory);
        publicContainer.put(PrebakeWebResourceAssemblerFactory.class, prebakeWebResourceAssemblerFactory);
        publicContainer.put(PluginResourceLocator.class, pluginResourceLocator);
        publicContainer.put(PageBuilderService.class, pageBuilderService);
        publicContainer.put(WebResourceUrlProvider.class, webResourceUrlProvider);

        publicContainer.put(Map.class, publicContainer);
        publicContainer.put(ModuleFactory.class, moduleFactory);
        publicContainer.put(PluginMetadataManager.class, new DefaultPluginMetadataManager());
        publicContainer.put(ServletContextFactory.class, simpleServletContextFactory);

        publicContainer.put(ModuleDescriptorFactory.class, moduleDescriptorFactory);
        publicContainer.put(ListableModuleDescriptorFactory.class, moduleDescriptorFactory);
        publicContainer.put(DescribedModuleDescriptorFactory.class, moduleDescriptorFactory);
        publicContainer.put(TenantAccessor.class, tenantAccessor);

        publicContainer.put(StaticTransformersSupplier.class, webResourceUrlProvider);

        publicContainer.put(ConnectionProvider.class, connectionProvider);

        publicContainer.put(HealthCheckWhitelist.class, new ClasspathFileHealthCheckWhitelist());

        hostContainer = new SimpleConstructorHostContainer(publicContainer);
        moduleFactory.addPrefixModuleFactory(new ClassPrefixModuleFactory(hostContainer));

        tenancyManager.enableTenancy();
        try {
            pluginLifecycle.earlyStartup();
            triggers.getPhase1Trigger().set(null);
        } catch (final PluginParseException e) {
            throw new RuntimeException(e);
        }
        downloadStrategies = new ArrayList<DownloadStrategy>();
        downloadStrategies.add(pluginDownloadStrategy);
    }

    private URL getBundledPluginDir(final ServletContext servletContext) {
        final File bundledPluginDir = makeSureDirectoryExists(servletContext, BUNDLED_PLUGINS_DIR);
        final URL bundledPluginUrl;
        try {
            bundledPluginUrl = bundledPluginDir.toURI().toURL();
        } catch (MalformedURLException e) {
            // This is a non-expected one as we know exactly the path should be valid.
            throw new IllegalStateException("Can't form url to bundled plugins directory at: " + BUNDLED_PLUGINS_DIR, e);
        }
        return bundledPluginUrl;
    }

    /**
     * The following futures allow asynchronous triggering of the plugin system. The first phase can occur immediately,
     * but the second phase must wait for the tenant to arrive and the first phase to complete. The plugin methods
     * involved are:
     */
    private class PluginPhaseTriggers {
        private SettableFuture<Void> triggerPhase1 = SettableFuture.create();
        private SettableFuture<Void> triggerTenant = SettableFuture.create();
        private ListenableFuture<List<Void>> phase2ready = Futures.allAsList(triggerPhase1, triggerTenant);

        public PluginPhaseTriggers() {
            Futures.addCallback(phase2ready, new FutureCallback<List<Void>>() {
                @Override
                public void onSuccess(List<Void> results) {
                    pluginLifecycle.lateStartup();
                }

                @Override
                public void onFailure(Throwable throwable) {
                }
            });
        }

        public SettableFuture<Void> getPhase1Trigger() {
            return triggerPhase1;
        }

        public SettableFuture<Void> getTenantTrigger() {
            return triggerTenant;
        }
    }

    private Application refapp() {
        return new Application() {
            @Override
            public String getKey() {
                return "refapp";
            }

            @Override
            public String getVersion() {
                return "1.0";
            }

            @Override
            public String getBuildNumber() {
                return "123";
            }
        };
    }

    private File findAndCreateDirectory(ServletContext servletContext, String sysPropName, String defaultPath) {
        File dir;
        if (System.getProperty(sysPropName) != null) {
            dir = makeSureDirectoryExists(System.getProperty(sysPropName));
        } else {
            dir = makeSureDirectoryExists(servletContext, defaultPath);
        }
        return dir;
    }

    private String determineVersion() {
        InputStream in = null;
        final Properties props = new Properties();
        try {
            in = getClass().getClassLoader().getResourceAsStream("META-INF/maven/com.atlassian.plugins/atlassian-plugins-core/pom.properties");
            if (in != null) {
                props.load(in);
                return props.getProperty("version");
            }
        } catch (final IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            IOUtils.closeQuietly(in);
        }
        return null;
    }

    private File makeSureDirectoryExists(final ServletContext servletContext, final String relativePath) {
        return makeSureDirectoryExists(servletContext.getRealPath(relativePath));
    }

    private File makeSureDirectoryExists(final String path) {
        final File dir = new File(path);
        if (!dir.exists() && !dir.mkdirs()) {
            throw new RuntimeException("Could not create directory <" + dir + ">");
        }
        return dir;
    }

    public static synchronized void setInstance(final ContainerManager mgr) {
        instance = mgr;
    }

    public static synchronized ContainerManager getInstance() {
        return instance;
    }

    public ServletModuleManager getServletModuleManager() {
        return servletModuleManager;
    }

    public OsgiContainerManager getOsgiContainerManager() {
        return osgiContainerManager;
    }

    public PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    public HostComponentProvider getHostComponentProvider() {
        return hostComponentProvider;
    }

    public ModuleDescriptorFactory getModuleDescriptorFactory() {
        return moduleDescriptorFactory;
    }

    public List<DownloadStrategy> getDownloadStrategies() {
        return downloadStrategies;
    }

    public PageBuilderService getPageBuilderService() {
        return pageBuilderService;
    }

    public WebResourceIntegration getWebResourceIntegration() {
        return webResourceIntegration;
    }

    void shutdown() {
        plugins.stop();
        connectionProvider.terminate();
    }

    private class SimpleHostComponentProvider implements HostComponentProvider {
        public void provide(final ComponentRegistrar componentRegistrar) {
            Assertions.notNull("publicContainer", publicContainer);
            for (final Map.Entry<Class<?>, Object> entry : publicContainer.entrySet()) {
                final String name = StringUtils.uncapitalize(entry.getKey().getSimpleName());
                componentRegistrar.register(entry.getKey()).forInstance(entry.getValue()).withName(name);
            }

        }
    }
}
