package com.atlassian.plugin.refimpl.filter;

import com.atlassian.plugin.refimpl.ContainerManager;
import com.atlassian.plugin.refimpl.tenant.RefappTenancyCondition;
import com.atlassian.plugin.refimpl.tenant.RefappTenantRegistry;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter will prevent access to the full application while it is still in a 'warm' untenanted state.
 */

public class RefAppTenantFilter implements Filter {
    private ServletContext context;
    private RefappTenantRegistry tenantRegistry;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.context = filterConfig.getServletContext();
        this.tenantRegistry = (RefappTenantRegistry) context.getAttribute(ContainerManager.TENANTACCESSOR);
    }

    /**
     * Captures the current {@link javax.servlet.ServletRequest} using the {@link com.atlassian.plugin.refimpl.CurrentHttpRequest}
     * class
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        if (RefappTenancyCondition.isEnabled()) {
            HttpServletRequest req = (HttpServletRequest) servletRequest;
            String path = req.getRequestURI();
            if (isBaseRest(path) || tenantRegistry.hasTenant()) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                ((HttpServletResponse) servletResponse).sendError(202, "Server unavailable. Waiting for tenant.");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
    }

    public boolean isBaseRest(String path) {
        String contextpath = context.getContextPath();
        return path.startsWith("/refapp/rest/landlord") ||
                path.startsWith("/refapp/plugins/servlet/landlord");
    }

}
