package com.atlassian.refapp.auth.internal.rest;

import com.atlassian.refapp.auth.internal.RefappPermissions;
import com.atlassian.refapp.auth.internal.UserContextHelper;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.user.EntityException;
import com.atlassian.user.GroupManager;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;
import com.atlassian.user.search.page.PagerUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import org.apache.commons.lang.StringUtils;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

import static com.google.common.base.MoreObjects.firstNonNull;
import static java.util.Arrays.asList;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.apache.commons.lang.StringUtils.capitalize;

/**
 * Simple REST resource to manage users in Refapp
 *
 * @since 3.0
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("users")
public class UserManagementResource {

    private final UserManager userManager;
    private final GroupManager groupManager;

    private final UserContextHelper userContextHelper;

    public UserManagementResource(AuthenticationContext authenticationContext, UserManager userManager,
                                  GroupManager groupManager) {
        this.userManager = userManager;
        this.groupManager = groupManager;

        this.userContextHelper = new UserContextHelper(authenticationContext, userManager, groupManager);
    }

    @GET
    @SuppressWarnings("unchecked")
    public Response getAllUsers() throws Exception {
        checkAdminPermission();

        List<User> users = PagerUtils.toList(userManager.getUsers());
        ImmutableList.Builder<RestUser> restUsers = ImmutableList.builder();
        for (User user : users) {
            restUsers.add(createRestUser(user, getPermissionLevel(user)));
        }

        return Response.ok(restUsers.build()).build();
    }

    @GET
    @Path("{username}")
    public Response getUser(@PathParam("username") String username) throws Exception {
        checkAdminPermission();
        checkUsername(username);

        User user = userManager.getUser(username);
        if (user == null) {
            return Response.status(NOT_FOUND)
                    .cacheControl(noCache())
                    .entity(new RestError(null, String.format("User with username '%s' not found", username)))
                    .build();
        }

        return Response.ok(createRestUser(user, getPermissionLevel(user))).build();
    }

    @PUT
    @Path("{username}")
    public Response createOrUpdateUser(@PathParam("username") String username, RestUser userData) throws Exception {
        checkAdminPermission();
        checkUsername(username);

        String email = firstNonNull(userData.email, username + "@example.com");
        String fullName = firstNonNull(userData.fullName, capitalize(username));
        RefappPermissions permissionLevel = firstNonNull(userData.permissionLevel, RefappPermissions.USER);

        User user = userManager.createUser(username);
        user.setEmail(email);
        user.setFullName(fullName);
        userManager.saveUser(user);
        userManager.alterPassword(user, username);

        for (RefappPermissions permission : RefappPermissions.values()) {
            if (permission.compareTo(permissionLevel) <= 0) {
                addMembership(permission.groupName(), user);
            }
        }

        // location will be set to the currently requested URI, which is correct URI for the created user
        return Response.created(URI.create("")).entity(createRestUser(user, permissionLevel)).build();
    }

    @DELETE
    @Path("{username}")
    public Response removeUser(@PathParam("username") String username) throws Exception {
        checkAdminPermission();
        checkUsername(username);

        User user = userManager.getUser(username);
        if (user != null) {
            userManager.removeUser(user);
        }

        return Response.noContent().build();
    }

    private void checkAdminPermission() {
        if (!userContextHelper.isRemoteUserSystemAdministrator() && !userContextHelper.isRemoteUserAdministrator()) {
            throw new WebApplicationException(Response.status(FORBIDDEN)
                    .entity(new RestError(null, "You need administrator permission to access this resource"))
                    .cacheControl(noCache())
                    .build());
        }
    }

    private void checkUsername(@PathParam("username") String username) {
        if (StringUtils.isEmpty(username)) {
            throw new WebApplicationException(Response.status(BAD_REQUEST)
                    .entity(new RestError("username", "Username not set"))
                    .cacheControl(noCache())
                    .build());
        }
    }

    private RestUser createRestUser(User user, RefappPermissions permissionLevel) {
        RestUser restUser = new RestUser();
        restUser.name = user.getName();
        restUser.fullName = user.getFullName();
        restUser.email = user.getEmail();
        restUser.permissionLevel = permissionLevel;

        return restUser;
    }

    private void addMembership(String groupName, User user) throws EntityException {
        groupManager.addMembership(groupManager.getGroup(groupName), user);
    }

    private RefappPermissions getPermissionLevel(User user) throws EntityException {
        for (RefappPermissions permission : Ordering.natural().reverse().immutableSortedCopy(asList(RefappPermissions.values()))) {
            if (isMember(user, permission.groupName())) {
                return permission;
            }
        }
        return RefappPermissions.USER;
    }

    private boolean isMember(User user, String group) throws EntityException {
        return groupManager.hasMembership(groupManager.getGroup(group), user);
    }

    private static CacheControl noCache() {
        CacheControl cacheControl = new CacheControl();
        cacheControl.setNoCache(true);
        cacheControl.setNoStore(true);
        return cacheControl;
    }
}
