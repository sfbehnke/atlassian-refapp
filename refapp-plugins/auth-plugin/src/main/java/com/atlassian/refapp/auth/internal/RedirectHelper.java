package com.atlassian.refapp.auth.internal;

import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RedirectHelper {
    public static void redirect(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // An empty context path actually means a "/" is needed to work as a relative redirect
        String location = StringUtils.isBlank(request.getContextPath()) ? "/" : request.getContextPath();

        String redirParam = request.getParameter("redir");
        if (!StringUtils.isEmpty(redirParam)) {
            if (redirParam.startsWith("http:") || redirParam.startsWith("https:")) {
                location = redirParam;
            } else {
                if (!redirParam.startsWith("/")) {
                    location += "/";
                }
                location += redirParam;
            }
        }

        response.sendRedirect(location);
    }

}
