package com.atlassian.refapp.auth.internal;

import com.atlassian.sal.api.user.UserRole;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.user.GroupManager;
import com.atlassian.user.UserManager;
import org.apache.velocity.VelocityContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

public class LoginServlet extends BaseVelocityServlet {
    private final Authenticator auth;
    private final UserContextHelper userHelper;

    public LoginServlet(Authenticator auth, AuthenticationContext authenticationContext, UserManager userManager, GroupManager groupManager) {
        super();
        this.auth = auth;
        this.userHelper = new UserContextHelper(authenticationContext, userManager, groupManager);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        VelocityContext context = createDefaultVelocityContext();
        context.put("redir", getContextRelativeRequestURL(request));

        final String requestedUserRole = request.getParameter("user_role");
        UserRole userRole = requestedUserRole != null ? UserRole.valueOf(requestedUserRole) : UserRole.USER;
        context.put("user_role", userRole.toString());

        context.put(userRole.toString().toLowerCase() + "Required", Boolean.TRUE);

        Principal user = auth.getUser(request);
        if (user == null) {
            context.put("loginURI", request.getContextPath() + "/plugins/servlet/login");
            getTemplate("/login.vm").merge(context, response.getWriter());
        } else {

            if (userRole.equals(UserRole.SYSADMIN) && !userHelper.isRemoteUserSystemAdministrator() ||
                    userRole.equals(UserRole.ADMIN) && !userHelper.isRemoteUserAdministrator()) {
                context.put("loginURI", request.getContextPath() + "/plugins/servlet/login");
                getTemplate("/login.vm").merge(context, response.getWriter());

                try {
                    auth.logout(request, response);
                } catch (AuthenticatorException e) {
                    throw new ServletException(e);
                }
            } else {
                response.sendRedirect(request.getContextPath() + "/plugins/servlet/users");
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RedirectHelper.redirect(req, resp);
    }

    private String getContextRelativeRequestURL(HttpServletRequest request) {
        if (request.getParameter("redir") != null) {
            return request.getParameter("redir");
        }

        StringBuilder redir = new StringBuilder(request.getServletPath());
        String path = request.getPathInfo();
        if (path != null && path.length() > 0) {
            redir.append(path);
        }
        String query = request.getQueryString();
        if (query != null && query.length() > 0) {
            redir.append('?').append(query);
        }
        return redir.toString();
    }
}
