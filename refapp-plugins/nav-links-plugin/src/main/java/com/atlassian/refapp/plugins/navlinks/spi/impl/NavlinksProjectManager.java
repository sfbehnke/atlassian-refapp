package com.atlassian.refapp.plugins.navlinks.spi.impl;

import com.atlassian.plugins.navlink.spi.Project;
import com.atlassian.plugins.navlink.spi.ProjectManager;
import com.atlassian.plugins.navlink.spi.ProjectNotFoundException;

/**
 * Dummy implementation, since the refapp doesn't allow maintenance of Custom Content Links
 */
public class NavlinksProjectManager implements ProjectManager {
    @Override
    public Project getProjectByKey(String s) throws ProjectNotFoundException {
        throw new ProjectNotFoundException(s);
    }
}