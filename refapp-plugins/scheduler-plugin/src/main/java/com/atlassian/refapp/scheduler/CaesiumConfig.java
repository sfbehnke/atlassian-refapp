package com.atlassian.refapp.scheduler;

import java.util.TimeZone;

import com.atlassian.scheduler.caesium.spi.CaesiumSchedulerConfiguration;

public final class CaesiumConfig implements CaesiumSchedulerConfiguration
{
    @Override
    public int refreshClusteredJobsIntervalInMinutes()
    {
        return 0;
    }

    @Override
    public int workerThreadCount()
    {
        return 10;
    }

    @Override
    public boolean useQuartzJobDataMapMigration()
    {
        return false;
    }

    @Override
    public TimeZone getDefaultTimeZone()
    {
        return TimeZone.getDefault();
    }

    @Override
    public boolean useFineGrainedSchedules()
    {
        return false;
    }
}