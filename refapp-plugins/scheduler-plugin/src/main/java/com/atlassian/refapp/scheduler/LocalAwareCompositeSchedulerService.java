package com.atlassian.refapp.scheduler;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.atlassian.refapp.scheduler.api.LocalAwareSchedulerService;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.ess.CompositeSchedulerService;
import com.atlassian.scheduler.ess.RemoteSchedulerProxy;
import com.atlassian.scheduler.status.JobDetails;

public class LocalAwareCompositeSchedulerService extends CompositeSchedulerService implements LocalAwareSchedulerService
{
    private final @Nonnull SchedulerService local;

    @Inject
    public LocalAwareCompositeSchedulerService(@Nonnull final SchedulerService local, @Nonnull final RemoteSchedulerProxy remote, final boolean btf)
    {
        super(local, remote, btf);
        this.local = local;
    }

    @Override
    public JobDetails getLocalJobDetails(JobId jobId)
    {
        return local.getJobDetails(jobId);
    }
}