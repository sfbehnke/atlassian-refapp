package com.atlassian.refapp.scheduler.api;

import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.status.JobDetails;

/**
 * This is for testing purpose, this service can access to the local scheduler service.
 */
public interface LocalAwareSchedulerService extends SchedulerService
{
    JobDetails getLocalJobDetails(JobId jobId);
}
