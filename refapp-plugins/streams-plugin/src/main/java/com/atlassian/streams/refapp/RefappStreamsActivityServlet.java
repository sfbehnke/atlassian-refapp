package com.atlassian.streams.refapp;

import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class RefappStreamsActivityServlet extends HttpServlet {
    private static final String TEMPLATE = "/templates/streams.vm";
    private final TemplateRenderer templateRenderer;

    public RefappStreamsActivityServlet(final TemplateRenderer templateRenderer) {
        this.templateRenderer = templateRenderer;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        final ImmutableMap.Builder<String, Object> contextBuilder = ImmutableMap.builder();

        contextBuilder.put("baseURL", getBaseUrl(request));

        render(TEMPLATE, contextBuilder.build(), response);
    }

    private void render(final String template, Map<String, Object> context, final HttpServletResponse response)
            throws IOException {
        response.setContentType("text/html; charset=utf-8");

        templateRenderer.render(template, context, response.getWriter());
    }

    private String getBaseUrl(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
