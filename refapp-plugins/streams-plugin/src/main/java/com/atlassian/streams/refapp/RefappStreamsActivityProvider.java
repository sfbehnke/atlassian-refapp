package com.atlassian.streams.refapp;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.StreamsActivityProvider;

import static com.atlassian.streams.api.common.Option.none;
import static com.google.common.base.Preconditions.checkNotNull;

public class RefappStreamsActivityProvider implements StreamsActivityProvider {
    private final RefappStreamsActivityManager streamsActivityManager;

    public RefappStreamsActivityProvider(final RefappStreamsActivityManager streamsActivityManager) {
        this.streamsActivityManager = checkNotNull(streamsActivityManager, "streamsActivityManager");
    }

    @Override
    public CancellableTask<StreamsFeed> getActivityFeed(final ActivityRequest activityRequest) throws StreamsException {
        return new CancellableTask<StreamsFeed>() {
            public StreamsFeed call() throws Exception {
                return new StreamsFeed("Activity Stream for RefApp", streamsActivityManager.getEntries(activityRequest), none(String.class));
            }

            public Result cancel() {
                return Result.CANCELLED;
            }
        };
    }

}
