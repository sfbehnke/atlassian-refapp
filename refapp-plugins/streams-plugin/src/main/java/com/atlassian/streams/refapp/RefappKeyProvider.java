package com.atlassian.streams.refapp;

import com.atlassian.streams.spi.StreamsKeyProvider;
import com.google.common.collect.ImmutableList;

public class RefappKeyProvider implements StreamsKeyProvider {

    public static final String REFAPP_KEY = "REFAPP";

    @Override
    public Iterable<StreamsKey> getKeys() {
        return ImmutableList.of(new StreamsKey(REFAPP_KEY, "Reference App"));
    }
}

