package com.atlassian.streams.refapp;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.ActivityOptions;
import com.atlassian.streams.spi.StreamsFilterOption;
import com.atlassian.streams.spi.StreamsFilterOptionProvider;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.ActivityObjectTypes.status;
import static com.atlassian.streams.api.ActivityVerbs.like;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableSet.of;
import static com.google.common.collect.Iterables.transform;

public class RefappFilterOptionProvider implements StreamsFilterOptionProvider {
    private static final Iterable<Pair<ActivityObjectType, ActivityVerb>> SUPPORT_ACTIVITIES = of(
            pair(article(), post()),
            pair(article(), update()),
            pair(article(), like()),
            pair(comment(), post()),
            pair(comment(), update()),
            pair(comment(), like()),
            pair(file(), post()),
            pair(file(), update()),
            pair(status(), update())
    );

    private final Function<Pair<ActivityObjectType, ActivityVerb>, ActivityOption> toActivityOption;

    public RefappFilterOptionProvider(final StreamsI18nResolver i18nResolver) {
        this.toActivityOption = ActivityOptions.toActivityOption(checkNotNull(i18nResolver, "i18nResolver"), "streams.filter.refapp");
    }

    @Override
    public Iterable<StreamsFilterOption> getFilterOptions() {
        // Standard filters is provided by the framework, we don't create specific filter options.
        return ImmutableList.of();
    }

    @Override
    public Iterable<StreamsFilterOptionProvider.ActivityOption> getActivities() {
        return transform(SUPPORT_ACTIVITIES, toActivityOption);
    }
}
