package com.atlassian.streams.refapp;

import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.refapp.api.StreamsActivityManager;
import com.atlassian.streams.spi.EntityResolver;

import static com.google.common.base.Preconditions.checkNotNull;

public class RefappEntityResolver implements EntityResolver {
    private StreamsActivityManager streamsActivityManager;

    public RefappEntityResolver(final StreamsActivityManager streamsActivityManager) {
        this.streamsActivityManager = checkNotNull(streamsActivityManager, "streamsActivityManager");
    }

    @Override
    public Option<Object> apply(String key) {
        return Option.<Object>option(streamsActivityManager.getEntry(Integer.valueOf(key)));
    }
}
