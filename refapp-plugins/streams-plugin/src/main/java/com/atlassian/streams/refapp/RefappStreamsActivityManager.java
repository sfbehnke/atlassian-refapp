package com.atlassian.streams.refapp;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.NonEmptyIterables;
import com.atlassian.streams.refapp.api.StreamsActivityManager;
import com.atlassian.streams.refapp.api.StreamsEntryRequest;
import com.atlassian.streams.spi.Filters;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

import java.net.URI;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.Filters.anyInUsers;
import static com.atlassian.streams.spi.Filters.entriesInActivities;
import static com.atlassian.streams.spi.Filters.entryAuthors;
import static com.atlassian.streams.spi.Filters.notInUsers;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.and;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.find;

/**
 * An in-memory storage service to developer to test features of activities streams in refapp, e.g. create entry, filter
 * entry....
 */
public class RefappStreamsActivityManager implements StreamsActivityManager {
    /**
     * Size of the memory buffer to store events.
     */
    public static final int BUFFER_SIZE = 100;

    private final ApplicationProperties applicationProperties;
    private final StreamsI18nResolver i18nResolver;
    private final RefappRenderer refappRenderer;

    /**
     * Array implementation of circular buffer.
     */
    private StreamsEntry[] buffer = new StreamsEntry[BUFFER_SIZE];
    /**
     * A global log to control concurrent access to the buffer.
     */
    private final Lock bufferLock = new ReentrantLock();
    /**
     * This keep position to put the next element into the buffer. After adding a new element, this position is increased or circled to the beginning when the buffer is full.
     */
    private int position = 0;

    public RefappStreamsActivityManager(ApplicationProperties applicationProperties,
                                        StreamsI18nResolver i18nResolver,
                                        RefappRenderer refappRenderer) {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.refappRenderer = checkNotNull(refappRenderer, "refappRenderer");
    }


    @Override
    public Iterable<StreamsEntry> getEntries(final ActivityRequest activityRequest) {
        // Standard filters: posted date and user
        // Provider filter: activity.
        final Iterable filteredResult = filter(getBufferSnapshot(), and(
                        inDateRange(activityRequest),
                        entryAuthors(notInUsers(activityRequest)),
                        entryAuthors(anyInUsers(activityRequest)),
                        entriesInActivities(activityRequest))
        );
        return take(activityRequest.getMaxResults(), filteredResult);
    }


    @Override
    public void addEntry(StreamsEntryRequest entryRequest) {
        bufferLock.lock();
        try {
            buffer[position] = createEntry(entryRequest);
            // Move to next position, circle at the end of the array.
            // With this, the next position will overwrite the oldest item when the buffer is full.
            position = (position + 1) % buffer.length;
        } finally {
            bufferLock.unlock();
        }
    }

    @Override
    public boolean removeEntry(final int id) {
        // After removing, we reorder the buffer by item's age decreasing, null elements are placed at the end of the buffer.
        // This is quite complicated, but it make we perform add operation more faster. Add is much more frequent operation, as expected.
        bufferLock.lock();
        try {
            final boolean isBufferCircled = buffer[position] != null;
            // Copy and uncircle the buffer if it is already circled.
            final StreamsEntry[] copy;
            if (isBufferCircled) {
                copy = copyUncircledBuffer();
            } else {
                copy = buffer;
            }

            // Find the position of the element to remove.
            int removePos = 0;
            Predicate<StreamsEntry> findEntry = findEntryById(id);
            for (; removePos < copy.length && copy[removePos] != null; removePos++) {
                if (findEntry.apply(copy[removePos])) {
                    break;
                }
            }
            if (removePos == copy.length || copy[removePos] == null) {
                return false;
            }

            // Shift back to remove the position and move the null element to the end of the array.
            int numberOfElements = isBufferCircled ? buffer.length : position;
            int shiftSize = numberOfElements - removePos - 1;
            System.arraycopy(copy, removePos + 1, copy, removePos, shiftSize);
            copy[removePos + shiftSize] = null;

            position = removePos + shiftSize;
            buffer = copy;
            return true;
        } finally {
            bufferLock.unlock();
        }
    }

    @Override
    public StreamsEntry getEntry(final int id) {
        return find(getBufferSnapshot(), findEntryById(id), null);
    }

    @Override
    public URI buildUriId(final int id) {
        return URI.create(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE) + "/refapp-streams/" + id);
    }

    private Predicate<StreamsEntry> findEntryById(final int id) {
        return new Predicate<StreamsEntry>() {
            @Override
            public boolean apply(final StreamsEntry input) {
                return input.getId().equals(buildUriId(id));
            }
        };
    }

    private StreamsEntry createEntry(StreamsEntryRequest entryInput) {
        return new StreamsEntry(StreamsEntry.params()
                .id(buildUriId(entryInput.getId()))
                .postedDate(entryInput.getPostedDate())
                .applicationType("com.atlassian.refimpl")
                .alternateLinkUri(buildUriId(entryInput.getId()))
                .authors(NonEmptyIterables.from(ImmutableList.of(new UserProfile.Builder(entryInput.getUser()).build())).get())
                .verb(entryInput.getVerb())
                .addActivityObject(new StreamsEntry.ActivityObject(StreamsEntry.ActivityObject.params()
                        .id("activity-object-" + entryInput.getId())
                        .activityObjectType(entryInput.getType())
                        .title(some(entryInput.getTitle()))))
                .renderer(refappRenderer), i18nResolver);
    }

    private Iterable<StreamsEntry> getBufferSnapshot() {
        // Take a snapshot of buffer before search on that snapshot to get rid of concurrent access on the buffer.
        final StreamsEntry[] copy;
        bufferLock.lock();
        try {
            // When the next position is non-null, it means that the buffer is already full and circled.
            final boolean isBufferCircled = buffer[position] != null;
            if (isBufferCircled) {
                // Copy the buffer and uncycle it in case it is full and circled.
                copy = copyUncircledBuffer();
            } else {
                // In case it is not full yet, we do want to exclude the null items at the end of the buffers.
                copy = new StreamsEntry[position];
                System.arraycopy(buffer, 0, copy, 0, position);
            }

        } finally {
            bufferLock.unlock();
        }
        return Arrays.asList(copy);
    }

    private StreamsEntry[] copyUncircledBuffer() {
        final StreamsEntry[] copy = new StreamsEntry[buffer.length];
        System.arraycopy(buffer, position, copy, 0, buffer.length - position);
        System.arraycopy(buffer, 0, copy, buffer.length - position, position);
        return copy;
    }

    private static Predicate<StreamsEntry> inDateRange(final ActivityRequest activityRequest) {
        return new Predicate<StreamsEntry>() {
            @Override
            public boolean apply(final StreamsEntry input) {
                if (input.getPostedDate() == null) {
                    return false;
                }
                return Filters.inDateRange(activityRequest).apply(input.getPostedDate().toDate());
            }
        };
    }
}
