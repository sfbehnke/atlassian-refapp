package com.atlassian.streams.refapp;


import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.refapp.api.StreamsEntryRequest;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nullable;
import java.util.Arrays;

import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityVerbs.like;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.AFTER;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BEFORE;
import static com.atlassian.streams.api.StreamsFilterType.Operator.BETWEEN;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.refapp.RefappStreamsActivityManager.BUFFER_SIZE;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.UPDATE_DATE;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.google.common.collect.Iterables.size;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RefappStreamsActivityManagerTest {
    private static final ImmutableMultimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> EMPTY_FILTER = ImmutableMultimap.<String, Pair<StreamsFilterType.Operator, Iterable<String>>>builder().build();
    private RefappStreamsActivityManager refappStreamsActivityManager;

    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private StreamsI18nResolver i18nResolver;
    @Mock
    private RefappRenderer refappRenderer;

    private DateTime today;

    @Before
    public void setup() {
        refappStreamsActivityManager = new RefappStreamsActivityManager(applicationProperties, i18nResolver, refappRenderer);
        today = new DateTime();
        // Fill the buffer:
        // Prepare N (0...N-1) entries that date are today, today + 1days, ..... today + N - 1, where N is the buffer size.
        // With first 5 entries are post, the rest are like activities.
        for (int i = 0; i < BUFFER_SIZE; i++) {
            final ActivityVerb verb = i < 5 ? post() : like();
            refappStreamsActivityManager.addEntry(new StreamsEntryRequest()
                    .id(i)
                    .title("Title " + i)
                    .postedDate(today.plusDays(i))
                    .type(article())
                    .verb(verb)
                    .user("User" + i));
        }

    }

    @Test
    public void getEntriesTestNoFilter() {
        // Restrict small result.
        getEntriesNoFilterAndAssert(5, 5);

        // With maximum result.
        getEntriesNoFilterAndAssert(BUFFER_SIZE, BUFFER_SIZE);
    }

    @Test
    public void getEntriesTestFilterDateDefore() {
        // Filter entries BEFORE(today + 2 days) => expect return 2 entries (today, today + 1)
        Pair<StreamsFilterType.Operator, Iterable<String>> dateFilter = pairDateFilter(BEFORE, today.plusDays(2));
        Iterable<StreamsEntry> entries = getStreamsEntriesWithDateFilter(dateFilter);

        assertThat(size(entries), is(2));
        // Assert the two entry is today (id = 0) and today + 1 (id=1)
        assertThat(find(entries, 0), notNullValue());
        assertThat(find(entries, 1), notNullValue());
    }

    @Test
    public void getEntriesTestFilterDateAfter() {
        // Filter entries AFTER(today + 2 days) => expect return BUFFER_SIZE - 3 entries (today + 3, today + 4 ... today + BUFFER - 1)
        Pair<StreamsFilterType.Operator, Iterable<String>> dateFilter = pairDateFilter(AFTER, today.plusDays(2));
        Iterable<StreamsEntry> entries = getStreamsEntriesWithDateFilter(dateFilter);

        assertThat(size(entries), is(BUFFER_SIZE - 3));
        // Assert the two entry is today (id = 0) and today + 1 (id=1)
        assertThat(find(entries, 2), nullValue());
        assertThat(find(entries, 3), notNullValue());
        assertThat(find(entries, BUFFER_SIZE - 1), notNullValue());
    }

    private Pair<StreamsFilterType.Operator, Iterable<String>> pairDateFilter(final StreamsFilterType.Operator operator, final DateTime... dates) {
        Iterable<String> dateStrings = Iterables.<DateTime, String>transform(Arrays.asList(dates), new Function<DateTime, String>() {
            @Override
            public String apply(@Nullable final DateTime input) {
                return String.valueOf(input.toDate().getTime());
            }
        });
        return Pair.<StreamsFilterType.Operator, Iterable<String>>pair(operator, dateStrings);
    }

    @Test
    public void getEntriesTestFilterDateBetween() {
        // Filter entries AFTER(today + 2 days, today + 5) => expect return 2 entries (today + 3, today + 4 )
        Pair<StreamsFilterType.Operator, Iterable<String>> dateFilter = pairDateFilter(BETWEEN, today.plusDays(2), today.plusDays(5));
        Iterable<StreamsEntry> entries = getStreamsEntriesWithDateFilter(dateFilter);

        assertThat(size(entries), is(2));
        assertThat(find(entries, 3), notNullValue());
        assertThat(find(entries, 4), notNullValue());
    }

    @Test
    public void getEntriesTestFilterUserIs() {
        Iterable<String> users = ImmutableList.of("User6");
        Pair<StreamsFilterType.Operator, Iterable<String>> dateFilter = pair(IS, users);
        Iterable<StreamsEntry> entries = getStreamsEntriesWithUserFilter(dateFilter);

        assertThat(size(entries), is(1));
        assertThat(find(entries, 6), notNullValue());
    }

    @Test
    public void getEntriesTestFilterUserIsNot() {
        Iterable<String> users = ImmutableList.of("User6");
        Pair<StreamsFilterType.Operator, Iterable<String>> dateFilter = pair(NOT, users);
        Iterable<StreamsEntry> entries = getStreamsEntriesWithUserFilter(dateFilter);

        assertThat(size(entries), is(BUFFER_SIZE - 1));
        assertThat(find(entries, 6), nullValue());
    }

    @Test
    public void getEntriesTestFilterActivity() {
        // Filter with type article post => assert 5 is found.
        Iterable<StreamsEntry> entries = getStreamsEntriesWithActivityFilter("article:post");
        assertThat(size(entries), is(5));

        // And the rest are article like activities.
        entries = getStreamsEntriesWithActivityFilter("article:like");
        assertThat(size(entries), is(BUFFER_SIZE - 5));
    }

    @Test
    public void addEntryTest() {
        // Add a new entry, while the buffer is already full.
        StreamsEntryRequest entryRequest = new StreamsEntryRequest()
                .id(BUFFER_SIZE)
                .postedDate(new DateTime().plusDays(BUFFER_SIZE))
                .title("Title" + BUFFER_SIZE)
                .user("User" + BUFFER_SIZE)
                .type(article())
                .verb(post());
        refappStreamsActivityManager.addEntry(entryRequest);

        // The oldest entry should be removed from buffer.
        assertThat(refappStreamsActivityManager.getEntry(0), nullValue());

        // The new entry should be found from the buffer.
        StreamsEntry entry = refappStreamsActivityManager.getEntry(entryRequest.getId());
        assertThat(entry, notNullValue());
        assertThat(entry.getPostedDate(), is(entryRequest.getPostedDate()));
        assertThat(entry.getVerb(), is(entryRequest.getVerb()));
        assertThat(entry.getActivityObjects().iterator().next().getActivityObjectType().get(), is(entryRequest.getType()));
        assertThat(entry.getAuthors().iterator().next().getUsername(), is(entryRequest.getUser()));
    }

    @Test
    public void removeEntryTest() {
        removeEntryAndAssert(5);

        getEntriesNoFilterAndAssert(BUFFER_SIZE, BUFFER_SIZE - 1);

        removeEntryAndAssert(6);

        getEntriesNoFilterAndAssert(BUFFER_SIZE, BUFFER_SIZE - 2);
    }

    private void removeEntryAndAssert(final int removedId) {
        assertThat(refappStreamsActivityManager.getEntry(removedId), notNullValue());
        assertThat(refappStreamsActivityManager.removeEntry(removedId), is(true));
        assertThat(refappStreamsActivityManager.getEntry(removedId), nullValue());
        assertThat(refappStreamsActivityManager.removeEntry(removedId), is(false));
    }

    private Iterable<StreamsEntry> getStreamsEntriesWithUserFilter(final Pair<StreamsFilterType.Operator, Iterable<String>> dateFilter) {
        ImmutableMultimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> standardFilters = ImmutableMultimap.<String, Pair<StreamsFilterType.Operator, Iterable<String>>>builder()
                .put(USER.getKey(), dateFilter).build();

        ActivityRequest request = mockActivityRequest(BUFFER_SIZE, standardFilters, EMPTY_FILTER);

        return refappStreamsActivityManager.getEntries(request);
    }

    private Iterable<StreamsEntry> getStreamsEntriesWithDateFilter(final Pair<StreamsFilterType.Operator, Iterable<String>> dateFilter) {
        ImmutableMultimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> standardFilters = ImmutableMultimap.<String, Pair<StreamsFilterType.Operator, Iterable<String>>>builder()
                .put(UPDATE_DATE.getKey(), dateFilter).build();

        ActivityRequest request = mockActivityRequest(BUFFER_SIZE, standardFilters, EMPTY_FILTER);

        return refappStreamsActivityManager.getEntries(request);
    }

    public StreamsEntry find(Iterable<StreamsEntry> entries, final int id) {
        return (StreamsEntry) Iterables.find(entries, new Predicate<StreamsEntry>() {
            @Override
            public boolean apply(@Nullable final StreamsEntry input) {
                return input.getId().equals(refappStreamsActivityManager.buildUriId(id));
            }
        }, null);
    }

    private ActivityRequest mockActivityRequest(final int maxResult,
                                                final Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> standardFilters,
                                                final Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> providerFilters) {
        ActivityRequest request = mock(ActivityRequest.class);
        when(request.getMaxResults()).thenReturn(maxResult);

        when(request.getStandardFilters()).thenReturn(standardFilters);
        when(request.getProviderFilters()).thenReturn(providerFilters);
        return request;
    }

    private void getEntriesNoFilterAndAssert(final int maxResult, final int expectedResult) {
        // Empty filter.
        ActivityRequest request = mockActivityRequest(maxResult, EMPTY_FILTER, EMPTY_FILTER);

        Iterable<StreamsEntry> entries = refappStreamsActivityManager.getEntries(request);
        assertThat(size(entries), is(expectedResult));
    }


    private Iterable<StreamsEntry> getStreamsEntriesWithActivityFilter(final String activity) {
        Iterable<String> activities = ImmutableList.of(activity);
        Pair<StreamsFilterType.Operator, Iterable<String>> activityFilter = pair(IS, activities);
        ImmutableMultimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> providerFilter = ImmutableMultimap.<String, Pair<StreamsFilterType.Operator, Iterable<String>>>builder()
                .put(ACTIVITY_KEY, activityFilter).build();

        ActivityRequest request = mockActivityRequest(BUFFER_SIZE, EMPTY_FILTER, providerFilter);

        return refappStreamsActivityManager.getEntries(request);
    }

}