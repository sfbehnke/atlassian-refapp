package com.atlassian.refapp.sal;

import com.atlassian.fugue.Pair;
import com.google.common.collect.ImmutableSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public abstract class FileBackedSettingsService {
    private static final Logger log = LoggerFactory.getLogger(FileBackedSettingsService.class);

    protected final File file;
    protected final Properties properties;

    public FileBackedSettingsService(Pair<File, Properties> fileAndProperties) {
        this.file = fileAndProperties.left();
        this.properties = fileAndProperties.right();
    }

    /**
     * In-memory settings, for testing only.
     */
    protected FileBackedSettingsService() {
        this.file = null;
        this.properties = new Properties();
    }

    @SuppressWarnings("AccessToStaticFieldLockedOnInstance")
    private synchronized void store() {
        if (this.file == null || !this.file.canWrite()) {
            // Read only settings
            return;
        }
        OutputStream os = null;
        try {
            os = new FileOutputStream(file);
            properties.storeToXML(os, "SAL Reference Implementation settings");
        } catch (IOException ioe) {
            log.error("Error storing properties", ioe);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException ioe) {
                    log.error("Error closing output stream", ioe);
                }
            }
        }
    }

    public class SettingsMap extends AbstractMap<String, String> {
        private final String settingsKey;

        public SettingsMap(String settingsKey) {
            if (settingsKey == null) {
                this.settingsKey = "global.";
            } else {
                this.settingsKey = "keys." + settingsKey + ".";
            }
        }

        @Nonnull
        public Set<Entry<String, String>> entrySet() {
            Set<Entry<String, String>> set = new HashSet<Entry<String, String>>();

            for (Entry<Object, Object> entry : properties.entrySet()) {
                final String key = (String) entry.getKey();
                if (key.startsWith(this.settingsKey)) {
                    set.add(new AbstractMap.SimpleEntry<String, String>(key.substring(this.settingsKey.length()), (String) entry.getValue()));
                }
            }

            return set;
        }

        public String get(Object key) {
            return properties.getProperty(settingsKey + key);
        }

        public String put(String key, String value) {
            String result = (String) properties.setProperty(settingsKey + key, value);
            store();
            return result;
        }

        public String remove(Object key) {
            String result = (String) properties.remove(settingsKey + key);
            store();
            return result;
        }

        public void clear() {
            for (Object object : ImmutableSet.copyOf(properties.keySet())) {
                if (object instanceof String && ((String) object).startsWith(settingsKey)) {
                    properties.remove(object);
                }
            }
            store();
        }
    }
}
