package com.atlassian.refapp.sal;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.refimpl.saldeps.ServletContextThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.refapp.sal.license.RefappProduct;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.google.common.base.Supplier;

import javax.annotation.Nonnull;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

/**
 * Implementation of ApplicationProperties
 */
@ExportAsService
@Named("applicationProperties")
public class RefimplApplicationProperties implements ApplicationProperties {
    private static final Date THEN = new Date();

    public static final Supplier<String> CANONICAL_BASE_URL_SUPPLIER = new Supplier<String>() {
        @Override
        public String get() {
            return getCanonicalBaseUrl();
        }
    };

    public static final Supplier<String> CANONICAL_CONTEXT_PATH_SUPPLIER = new Supplier<String>() {
        @Override
        public String get() {
            return getCanonicalContextPath();
        }
    };

    @Override
    @Deprecated
    public String getBaseUrl() {
        Option<String> absoluteUrl = getAbsoluteBaseUrlFromRequest();
        return absoluteUrl.getOrElse(CANONICAL_BASE_URL_SUPPLIER);
    }

    private Option<String> getAbsoluteBaseUrlFromRequest() {
        HttpServletRequest request = ServletContextThreadLocal.getRequest();
        if (request != null) {
            return Option.some(HttpServletRequestBaseUrlExtractor.extractBaseUrl(request));
        }
        return Option.none();
    }

    private Option<String> getContextPathFromRequest() {
        HttpServletRequest request = ServletContextThreadLocal.getRequest();
        if (request != null) {
            return Option.some(request.getContextPath());
        }
        return Option.none();
    }

    private static String getCanonicalBaseUrl() {
        String baseurl = System.getProperty("baseurl");
        if (baseurl == null)
            throw new IllegalStateException("baseurl system property is not configured");
        return baseurl;
    }

    private static String getCanonicalContextPath() {
        String baseUrl = getCanonicalBaseUrl();
        try {
            return new URL(baseUrl).getPath();
        } catch (MalformedURLException e) {
            throw new IllegalStateException("Base URL misconfigured", e);
        }
    }

    @Override
    @Nonnull
    public String getBaseUrl(UrlMode urlMode) {
        switch (urlMode) {
            case ABSOLUTE: {
                Option<String> absoluteUrl = getAbsoluteBaseUrlFromRequest();
                return absoluteUrl.getOrElse(CANONICAL_BASE_URL_SUPPLIER);
            }
            case CANONICAL:
                return getCanonicalBaseUrl();
            case RELATIVE: {
                Option<String> contextPath = getContextPathFromRequest();
                return contextPath.getOrElse(CANONICAL_CONTEXT_PATH_SUPPLIER);
            }
            case RELATIVE_CANONICAL:
                return getCanonicalContextPath();
            case AUTO: {
                Option<String> contextPath = getContextPathFromRequest();
                return contextPath.getOrElse(CANONICAL_BASE_URL_SUPPLIER);
            }
            default:
                throw new IllegalStateException("Unhandled UrlMode " + urlMode);
        }
    }

    @Override
    @Nonnull
    public String getDisplayName() {
        return RefappProduct.REFAPP.getName();
    }

    @Override
    @Nonnull
    public String getPlatformId() {
        return RefappProduct.REFAPP.getNamespace();
    }

    @Override
    @Nonnull
    public String getVersion() {
        return "1.0";
    }

    @Override
    @Nonnull
    public Date getBuildDate() {
        return THEN;
    }

    @Override
    @Nonnull
    public String getBuildNumber() {
        return "123";
    }

    @Override
    @Nonnull
    public File getHomeDirectory() {
        return new File(System.getProperty("refapp.home", System.getProperty("java.io.tmpdir")));
    }

    @Override
    @Deprecated
    public String getPropertyValue(String s) {
        return null;
    }
}
