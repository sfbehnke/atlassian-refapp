package com.atlassian.refapp.sal;

import com.atlassian.fugue.Pair;
import com.atlassian.sal.api.ApplicationProperties;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FileBackedSettingsFileFactory {
    private static Logger log = LoggerFactory.getLogger(FileBackedSettingsFileFactory.class);

    public static Pair<File, Properties> getFileAndProperties(String fileStorageProperty, String defaultFileName, boolean useMemoryStore, ApplicationProperties applicationProperties) {
        File file = new File(System.getProperty(fileStorageProperty, defaultFileName));
        Properties properties = new Properties();
        if (useMemoryStore) {
            log.info("Using memory store for settings");
            file = null;
        } else if (!file.exists() || !file.canRead()) {
            // Use refapp home directory
            File dataDir = new File(applicationProperties.getHomeDirectory(), "data");
            try {
                if (!dataDir.exists()) {
                    dataDir.mkdirs();
                }
                file = new File(dataDir, defaultFileName);
                file.createNewFile();
            } catch (IOException ioe) {
                log.warn("Error creating settings properties, using memory store", ioe);
                file = null;
            }
        }
        if (file != null && file.length() > 0) {
            file = load(file, properties);
        }
        if (file != null) {
            // File is a new file
            log.info("Using " + file.getAbsolutePath() + " as settings store");
        }
        return Pair.pair(file, properties);
    }

    private static File load(File file, Properties properties) {
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            properties.loadFromXML(is);
        } catch (Exception e) {
            log.error("Error loading settings properties, using memory store", e);
            IOUtils.closeQuietly(is);
            return null;
        }
        return file;
    }
}
