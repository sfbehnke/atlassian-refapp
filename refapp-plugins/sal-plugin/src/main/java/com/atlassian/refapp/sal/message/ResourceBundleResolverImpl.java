package com.atlassian.refapp.sal.message;

import javax.inject.Named;
import java.util.Locale;
import java.util.ResourceBundle;

@Named("resourceBundleResolver")
class ResourceBundleResolverImpl implements ResourceBundleResolver {
    public ResourceBundle getBundle(String bundleName, Locale locale, ClassLoader classLoader) {
        return ResourceBundle.getBundle(bundleName, locale, classLoader);
    }
}
