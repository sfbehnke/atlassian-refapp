package com.atlassian.refapp.sal.license;

import com.atlassian.extras.api.AtlassianLicense;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.extras.api.LicenseManager;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.core.AtlassianLicenseFactory;
import com.atlassian.extras.core.DefaultAtlassianLicenseFactory;
import com.atlassian.extras.core.DefaultLicenseManager;
import com.atlassian.extras.core.ProductLicenseFactory;
import com.atlassian.extras.decoder.api.LicenseDecoder;
import com.atlassian.extras.decoder.v2.Version2LicenseDecoder;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.license.BaseLicenseDetails;
import com.atlassian.sal.api.license.LicenseHandler;
import com.atlassian.sal.api.license.MultiProductLicenseDetails;
import com.atlassian.sal.api.license.ProductLicense;
import com.atlassian.sal.api.license.SingleProductLicenseDetailsView;
import com.atlassian.sal.api.validate.ValidationResult;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Named;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.google.common.collect.Collections2.transform;

/**
 * Simple implementation of license handler
 */
@ExportAsService
@Named("licenseHandler")
public class RefimplLicenseHandler implements LicenseHandler {
    private static final Log log = LogFactory.getLog(RefimplLicenseHandler.class);

    private final LicenseManager licenseManager;

    private String license;

    public RefimplLicenseHandler() {
        final LicenseDecoder licenseDecoder = new Version2LicenseDecoder();
        final ProductLicenseFactory productLicenseFactory = new RefappProduct.RefappProductLicenseFactory();
        final Map<Product, ProductLicenseFactory> licenseMap = ImmutableMap.of(RefappProduct.REFAPP, productLicenseFactory);
        final AtlassianLicenseFactory atlassianLicenseFactory = new DefaultAtlassianLicenseFactory(licenseMap);
        licenseManager = new DefaultLicenseManager(licenseDecoder, atlassianLicenseFactory);
    }

    /**
     * Sets the license, going through the regular validation steps as if you used the web UI
     *
     * @param license The license string
     */
    @Override
    @Deprecated
    public void setLicense(final String license) {
        addProductLicense(RefappProduct.REFAPP.getNamespace(), license);
    }


    @Override
    public boolean hostAllowsMultipleLicenses() {
        return false;
    }

    @Override
    public boolean hostAllowsCustomProducts() {
        return false;
    }

    @Override
    public Set<String> getProductKeys() {
        return ImmutableSet.of(RefappProduct.REFAPP.getNamespace());
    }

    @Override
    public void addProductLicense(@Nonnull final String productKey, @Nonnull final String license) {
        final String refappProductId = RefappProduct.REFAPP.getNamespace();
        if (!refappProductId.equals(productKey)) {
            throw new IllegalArgumentException("Refapp only supports '" + refappProductId + "' licenses");
        }

        log.info("Setting license " + license);
        final ValidationResult validationResult = validateProductLicense(refappProductId, license, null);

        if (validationResult.isValid()) {
            this.license = license;
        } else {
            final String message = StringUtils.join(validationResult.getErrorMessages().iterator(), ", ");
            throw new IllegalArgumentException("Invalid License: " + message);
        }
    }

    @Override
    public void removeProductLicense(@Nonnull final String productKey) {
        if (RefappProduct.REFAPP.getNamespace().equals(productKey)) {
            throw new IllegalStateException("Cannot remove last license from Refapp");
        }
        // else just do nothing - removing a license we don't have is a no-op
    }

    @Override
    @Nonnull
    public ValidationResult validateProductLicense(
            @Nonnull final String productKey,
            @Nonnull final String license,
            @Nullable final Locale locale) {
        try {
            if (!RefappProduct.REFAPP.getNamespace().equals(productKey)) {
                return ValidationResult.withErrorMessages(ImmutableList.of("Unknown product '" + productKey + "'"));
            }

            final AtlassianLicense atlassianLicense = licenseManager.getLicense(license);
            if (null == atlassianLicense) {
                return ValidationResult.withErrorMessages(ImmutableList.of("Failed to parse license '" + license + "'"));
            }

            // It feels like we should check the license contains a Refapp license, but the default license in the CTK doesn't,
            // and consequently Refapp fails the CTK if we check for this.
            // if (null == atlassianLicense.getProductLicense(RefappProduct.REFAPP))
            // {
            //     return ValidationResult.withErrorMessages(ImmutableList.of("No License for '" + RefappProduct.REFAPP + "'"));
            // }

            return ValidationResult.valid();
        } catch (final LicenseException el) {
            return ValidationResult.withErrorMessages(ImmutableList.of(el.getMessage()));
        }
    }

    /**
     * Gets the server ID of the currently running application.
     *
     * @return the server ID.
     */
    @Override
    @Nullable
    public String getServerId() {
        return "AREF-IMPL-SERV-ERID";
    }

    /**
     * Gets the Support Entitlement Number (SEN) for the currently running application.
     *
     * @return the Support Entitlement Number, or {@code null} if there is no current support entitlement.
     */
    @Override
    @Nullable
    @Deprecated
    public String getSupportEntitlementNumber() {
        return null;
    }

    @Nonnull
    @Override
    public SortedSet<String> getAllSupportEntitlementNumbers() {
        return new TreeSet<String>();
    }

    @Override
    @Nullable
    public String getRawProductLicense(final String productKey) {
        return (RefappProduct.REFAPP.getNamespace().equals(productKey)) ? license : null;
    }

    @Override
    @Nullable
    public SingleProductLicenseDetailsView getProductLicenseDetails(@Nonnull final String productKey) {
        final AtlassianLicense atlassianLicense = licenseManager.getLicense(license);
        for (final com.atlassian.extras.api.ProductLicense productLicense : atlassianLicense.getProductLicenses()) {
            if (productLicense.getProduct().getNamespace().equals(productKey)) {
                return new SingleProductLicenseDetailsViewImpl(productLicense);
            }
        }
        return null;
    }

    @Override
    @Nonnull
    public Collection<MultiProductLicenseDetails> getAllProductLicenses() {
        return ImmutableList.of(decodeLicenseDetails(license));
    }

    @Override
    @Nonnull
    public MultiProductLicenseDetails decodeLicenseDetails(@Nonnull final String licenseString) {
        final AtlassianLicense atlassianLicense = licenseManager.getLicense(licenseString);
        final com.atlassian.extras.api.ProductLicense refappLicense = atlassianLicense.getProductLicense(RefappProduct.REFAPP);
        if (null == refappLicense) {
            throw new IllegalArgumentException("Cannot decode license '" + licenseString + "'");
        }

        return new MultiProductLicenseDetailsImpl(atlassianLicense, refappLicense);
    }

    private static class BaseLicenseDetailsImpl implements BaseLicenseDetails {
        private final com.atlassian.extras.api.ProductLicense extrasProductLicense;

        BaseLicenseDetailsImpl(final com.atlassian.extras.api.ProductLicense extrasProductLicense) {
            this.extrasProductLicense = extrasProductLicense;
        }

        public com.atlassian.extras.api.ProductLicense getExtrasProductLicense() {
            return extrasProductLicense;
        }

        @Override
        public boolean isEvaluationLicense() {
            return extrasProductLicense.isEvaluation();
        }

        @Override
        @Nonnull
        public String getLicenseTypeName() {
            return extrasProductLicense.getLicenseType().name();
        }

        @Override
        public String getOrganisationName() {
            return extrasProductLicense.getOrganisation().getName();
        }

        @Override
        @Nullable
        public String getSupportEntitlementNumber() {
            return extrasProductLicense.getSupportEntitlementNumber();
        }

        @Override
        public String getDescription() {
            return extrasProductLicense.getDescription();
        }

        @Override
        public String getServerId() {
            return extrasProductLicense.getServerId();
        }

        @Override
        public boolean isPerpetualLicense() {
            return (null == extrasProductLicense.getExpiryDate());
        }

        @Override
        @Nullable
        public Date getLicenseExpiryDate() {
            return extrasProductLicense.getExpiryDate();
        }

        @Override
        @Nullable
        public Date getMaintenanceExpiryDate() {
            return extrasProductLicense.getMaintenanceExpiryDate();
        }

        @Override
        public boolean isDataCenter() {
            return extrasProductLicense.isClusteringEnabled();
        }

        @Override
        public boolean isEnterpriseLicensingAgreement() {
            // No analogue in "extras" licenses
            return false;
        }

        @Override
        @Nullable
        public String getProperty(@Nonnull final String key) {
            return extrasProductLicense.getProperty(key);
        }
    }

    private static class SingleProductLicenseDetailsViewImpl
            extends BaseLicenseDetailsImpl
            implements SingleProductLicenseDetailsView {
        public SingleProductLicenseDetailsViewImpl(final com.atlassian.extras.api.ProductLicense extrasProductLicense) {
            super(extrasProductLicense);
        }

        @Override
        @Nonnull
        public String getProductKey() {
            return getExtrasProductLicense().getProduct().getNamespace();
        }

        @Override
        public boolean isUnlimitedNumberOfUsers() {
            return getExtrasProductLicense().isUnlimitedNumberOfUsers();
        }

        @Override
        public int getNumberOfUsers() {
            return getExtrasProductLicense().getMaximumNumberOfUsers();
        }

        @Override
        @Nonnull
        public String getProductDisplayName() {
            return getExtrasProductLicense().getProduct().getName();
        }
    }

    private static class MultiProductLicenseDetailsImpl
            extends BaseLicenseDetailsImpl
            implements MultiProductLicenseDetails {
        private final AtlassianLicense atlassianLicense;

        public MultiProductLicenseDetailsImpl(
                final AtlassianLicense atlassianLicense,
                final com.atlassian.extras.api.ProductLicense extrasProductLicense) {
            super(extrasProductLicense);
            this.atlassianLicense = atlassianLicense;
        }

        @Nonnull
        @Override
        public Set<ProductLicense> getProductLicenses() {
            return ImmutableSet.copyOf(
                    transform(atlassianLicense.getProductLicenses(),
                            new Function<com.atlassian.extras.api.ProductLicense, com.atlassian.sal.api.license.ProductLicense>() {
                                @Override
                                public com.atlassian.sal.api.license.ProductLicense
                                apply(@Nullable final com.atlassian.extras.api.ProductLicense extrasProductLicense) {
                                    return (null == extrasProductLicense)
                                            ? null
                                            : new SingleProductLicenseDetailsViewImpl(extrasProductLicense);
                                }
                            })
            );
        }

        @Nonnull
        @Override
        public Set<ProductLicense> getEmbeddedLicenses() {
            return ImmutableSet.of();
        }

        @Override
        @Nullable
        public com.atlassian.sal.api.license.ProductLicense getProductLicense(@Nonnull final String productKey) {
            for (final com.atlassian.sal.api.license.ProductLicense salProductLicense : getProductLicenses()) {
                if (salProductLicense.getProductKey().equals(productKey)) {
                    return salProductLicense;
                }
            }

            return null;
        }

    }
}
