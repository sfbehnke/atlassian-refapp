package com.atlassian.refapp.sal.executor;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.executor.ThreadLocalContextManager;
import com.atlassian.seraph.auth.AuthenticationContext;

import javax.inject.Inject;
import javax.inject.Named;
import java.security.Principal;

@ExportAsService
@Named("refimplThreadLocalContextManager")
public class RefimplThreadLocalContextManager implements ThreadLocalContextManager<Principal> {
    private final AuthenticationContext authenticationContext;

    @Inject
    public RefimplThreadLocalContextManager(AuthenticationContext authenticationContext) {
        this.authenticationContext = authenticationContext;
    }

    /**
     * Get the thread local context of the current thread
     *
     * @return The thread local context
     */
    public Principal getThreadLocalContext() {
        return authenticationContext.getUser();
    }

    /**
     * Set the thread local context on the current thread
     *
     * @param context The context to set
     */
    public void setThreadLocalContext(Principal context) {
        authenticationContext.setUser(context);
    }

    /**
     * Clear the thread local context on the current thread
     */
    public void clearThreadLocalContext() {
        authenticationContext.setUser(null);
    }
}
