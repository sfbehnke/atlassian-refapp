package com.atlassian.refapp.sal.websudo;

import com.atlassian.oauth.util.RequestAnnotations;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.refapp.auth.external.WebSudoSessionManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

import static com.google.common.base.Preconditions.checkNotNull;

@ExportAsService
@Named("salWebSudoManager")
public final class RefImplWebSudoManager implements WebSudoManager {
    private static final String WEBSUDO_PATH = "/plugins/servlet/websudo";
    private static final String WEBSUOD_REQUEST_ATTR = WebSudoManager.class.getName() + "-websudo-resource";

    private final WebSudoSessionManager webSudoAuthenticator;

    @Inject
    public RefImplWebSudoManager(final WebSudoSessionManager webSudoAuthenticator) {
        this.webSudoAuthenticator = checkNotNull(webSudoAuthenticator);
    }

    public boolean canExecuteRequest(HttpServletRequest request) {
        return RequestAnnotations.isOAuthRequest(request) || webSudoAuthenticator.isWebSudoSession(request);
    }

    public void enforceWebSudoProtection(HttpServletRequest request, HttpServletResponse response) {
        try {
            final String queryString = request.getQueryString();
            final String requestURI = request.getServletPath();
            final String pathInfo = request.getPathInfo();
            response.sendRedirect(request.getContextPath()
                    + WEBSUDO_PATH
                    + "?redir="
                    + URLEncoder.encode(requestURI +
                    ((null != pathInfo) ? pathInfo : "") +
                    ((null != queryString) ? "?" + queryString : ""), "UTF-8"));
        } catch (IOException e) {
            throw new SecurityException("Failed to redirect to " + WEBSUDO_PATH);
        }
    }

    public void willExecuteWebSudoRequest(HttpServletRequest request) throws WebSudoSessionException {
        if (null == request || !canExecuteRequest(request)) {
            throw new WebSudoSessionException();
        }
        request.setAttribute(WEBSUOD_REQUEST_ATTR, Boolean.TRUE);
    }
}
