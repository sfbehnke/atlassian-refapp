package com.atlassian.refapp.sal.project;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.project.ProjectManager;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@ExportAsService
@Named("RefimplProjectManager")
public class RefimplProjectManager implements ProjectManager {
    private static final String CHARLIE_KEYS = "charlie.keys";

    private final PluginSettingsFactory pluginSettingsFactory;

    @Inject
    public RefimplProjectManager(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    /**
     * Get all project keys
     *
     * @return All the project keys
     */
    public Collection<String> getAllProjectKeys() {
        List<String> charlies = (List<String>) pluginSettingsFactory.createGlobalSettings().get(CHARLIE_KEYS);
        if (charlies == null) {
            charlies = new ArrayList<String>();
        }
        return charlies;
    }

}
