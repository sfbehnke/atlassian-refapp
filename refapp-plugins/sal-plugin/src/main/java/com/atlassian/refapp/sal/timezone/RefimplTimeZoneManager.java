package com.atlassian.refapp.sal.timezone;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.timezone.TimeZoneManager;
import com.atlassian.sal.api.user.UserKey;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.inject.Named;
import java.util.TimeZone;

@ExportAsService
@Named("timeZoneManager")
public class RefimplTimeZoneManager implements TimeZoneManager {
    @Nonnull
    public TimeZone getUserTimeZone() {
        return getDefaultTimeZone();
    }

    @Nonnull
    public TimeZone getDefaultTimeZone() {
        return TimeZone.getDefault();
    }

    @Override
    @Nonnull
    public TimeZone getUserTimeZone(@Nonnull UserKey user) {
        Preconditions.checkNotNull(user);
        return getDefaultTimeZone();
    }
}
