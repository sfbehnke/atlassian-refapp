package com.atlassian.refapp.sal.internal;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;


import com.atlassian.plugin.refimpl.saldeps.CookieBasedScopeManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.refapp.auth.external.WebSudoSessionManager;
import com.atlassian.sal.api.rdbms.TransactionalExecutorFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.atlassian.sal.core.auth.OAuthRequestVerifierFactoryImpl;
import com.atlassian.sal.core.auth.SeraphAuthenticationListener;
import com.atlassian.sal.core.auth.SeraphLoginUriProvider;
import com.atlassian.sal.core.component.DefaultComponentLocator;
import com.atlassian.sal.core.executor.DefaultThreadLocalDelegateExecutorFactory;
import com.atlassian.sal.core.features.DefaultDarkFeatureManager;
import com.atlassian.sal.core.features.DefaultSiteDarkFeaturesStorage;
import com.atlassian.sal.core.net.HttpClientRequestFactory;
import com.atlassian.sal.core.rdbms.DefaultTransactionalExecutorFactory;
import com.atlassian.sal.core.scheduling.TimerPluginScheduler;
import com.atlassian.sal.core.transaction.HostContextTransactionTemplate;
import com.atlassian.sal.core.xsrf.IndependentXsrfTokenAccessor;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;
import com.atlassian.security.auth.trustedapps.BouncyCastleEncryptionProvider;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.RoleMapper;
import com.atlassian.user.GroupManager;
import com.atlassian.user.UserManager;
import com.atlassian.user.security.authentication.Authenticator;

import javax.inject.Named;

/**
 * Host Atlassian Spring Scanner @ComponentImport annotations.
 *
 * @since 3.0.0
 */
@SuppressWarnings("UnusedDeclaration")
public class ComponentImports {
    @ComponentImport
    PluginEventManager pluginEventManager;
    @ComponentImport
    PluginAccessor pluginAccessor;
    @ComponentImport
    WebSudoSessionManager webSudoSessionManager;
    @ComponentImport
    RoleMapper roleMapper;
    @ComponentImport
    Authenticator authenticator;
    @ComponentImport("atlassianUserManager")
    UserManager atlassianUserManager;
    @ComponentImport
    GroupManager groupManager;
    @ComponentImport
    AuthenticationContext authenticationContext;
    @ComponentImport
    ClusterLockService clusterLockService;
    @ComponentImport
    ConnectionProvider connectionProvider;

    @ExportAsService
    @Named("authListener")
    SeraphAuthenticationListener seraphAuthenticationListener;
    @ExportAsService
    @Named("componentLocator")
    DefaultComponentLocator defaultComponentLocator;
    @ExportAsService
    @Named("darkFeatureManager")
    DefaultDarkFeatureManager defaultDarkFeatureManager;

    @ExportAsService
    @Named("scopeManager")
    CookieBasedScopeManager scopeManager;

    @ExportAsService
    @Named("loginUriProvider")
    SeraphLoginUriProvider seraphLoginUriProvider;
    @ExportAsService
    @Named("noopTransactionTemplate")
    HostContextTransactionTemplate hostContextTransactionTemplate;
    @ExportAsService
    @Named("oAuthRequestVerifierFactory")
    OAuthRequestVerifierFactoryImpl oAuthRequestVerifierFactoryImpl;
    @ExportAsService(PluginScheduler.class)
    @Named("pluginScheduler")
    TimerPluginScheduler timerPluginScheduler;
    @ExportAsService
    @Named("refimplThreadLocalDelegateExecutorFactory")
    DefaultThreadLocalDelegateExecutorFactory
            defaultThreadLocalDelegateExecutorFactory;
    @ExportAsService
    @Named("requestFactory")
    HttpClientRequestFactory httpClientRequestFactory;
    @ExportAsService
    @Named("xsrfRequestValidator")
    XsrfRequestValidatorImpl xsrfRequestValidator;
    @ExportAsService
    @Named("xsrfTokenAccessor")
    IndependentXsrfTokenAccessor independentXsrfTokenAccessor;

    @Named("siteDarkFeaturesStorage")
    DefaultSiteDarkFeaturesStorage defaultSiteDarkFeaturesStorage;
    @Named("encryption-provider")
    BouncyCastleEncryptionProvider encryptionProvider;

    @ExportAsService(TransactionalExecutorFactory.class)
    @Named("transactionalExecutorFactory")
    DefaultTransactionalExecutorFactory transactionalExecutorFactory;
}
