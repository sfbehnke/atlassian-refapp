package com.atlassian.refapp.sal.rdbms;

import com.atlassian.fugue.Option;
import com.atlassian.refapp.api.ConnectionProvider;
import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.api.rdbms.RdbmsException;
import com.atlassian.sal.spi.HostConnectionAccessor;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import java.sql.Connection;
import java.sql.SQLException;

@SuppressWarnings("unused")
@Named("refImplHostConnectionAccessor")
public class RefImplHostConnectionAccessor implements HostConnectionAccessor {

    private final ConnectionProvider connectionProvider;

    @Inject
    public RefImplHostConnectionAccessor(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    @Override
    public <A> A execute(boolean readOnly, boolean newTransaction, @Nonnull ConnectionCallback<A> callback) {
        // retrieve and sanitise the connection
        try (final Connection connection = connectionProvider.connection()) {
            connection.setAutoCommit(false);
            connection.setReadOnly(readOnly);

            // we can't support execution in existing or new transaction context, because refapp doesn't have one
            return callback.execute(connection);
        } catch (SQLException e) {

            // not much we can do here but throw, so that a rollback occurs
            throw new RdbmsException("refapp unable to execute against database connection", e);
        }
    }

    @Nonnull
    @Override
    public Option<String> getSchemaName() {
        return Option.option(connectionProvider.schema().orElse(null));
    }
}
