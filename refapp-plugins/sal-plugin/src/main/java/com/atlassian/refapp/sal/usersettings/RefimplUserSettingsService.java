package com.atlassian.refapp.sal.usersettings;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.refapp.sal.FileBackedSettingsFileFactory;
import com.atlassian.refapp.sal.FileBackedSettingsService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.atlassian.sal.core.usersettings.DefaultUserSettings;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import org.apache.log4j.Logger;

import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

import static com.google.common.base.Preconditions.checkNotNull;

@ExportAsService
@Named("userSettingsService")
public class RefimplUserSettingsService extends FileBackedSettingsService implements UserSettingsService {
    private static final Logger log = Logger.getLogger(RefimplUserSettingsService.class);

    private final UserManager userManager;

    @Inject
    public RefimplUserSettingsService(UserManager userManager, ApplicationProperties applicationProperties) {
        super(FileBackedSettingsFileFactory.getFileAndProperties("sal.com.atlassian.refapp.sal.usersettings.store", "com.atlassian.refapp.sal.usersettings.xml", Boolean.valueOf(System.getProperty("sal.com.atlassian.refapp.sal.usersettings.usememorystore", "false")), applicationProperties));
        this.userManager = userManager;
    }

    @VisibleForTesting
    RefimplUserSettingsService(UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public UserSettings getUserSettings(String username) {
        if (userManager.resolve(username) == null) {
            throw new IllegalArgumentException("No user exists with username " + username);
        }

        SettingsMap settingsMap = new SettingsMap(username);
        UserSettingsBuilder settings = UserSettingsBuilderRefAppImpl.builder();
        for (String key : settingsMap.keySet()) {
            final String value = settingsMap.get(key);
            if (value.startsWith("B")) {
                settings.put(key, Boolean.parseBoolean(value.substring(1)));
            } else if (value.startsWith("L")) {
                settings.put(key, Long.parseLong(value.substring(1)));
            } else if (value.startsWith("S")) {
                settings.put(key, value.substring(1));
            } else {
                log.warn("Found user setting with unknown prefix. User: " + username + ", key: " + key + ", value: " + value);
            }
        }

        return settings.build();
    }

    @Override
    public UserSettings getUserSettings(UserKey userKey) {
        checkNotNull(userKey, "userKey cannot be null");

        return getUserSettings(userKey.getStringValue());
    }

    @Override
    public synchronized void updateUserSettings(String username, Function<UserSettingsBuilder, UserSettings> updateFunction) {
        final UserSettings userSettings = updateFunction.apply(UserSettingsBuilderRefAppImpl.builder(getUserSettings(username)));

        // check for keys being too long, or values being too long
        for (String key : userSettings.getKeys()) {
            if (key.length() > UserSettingsService.MAX_KEY_LENGTH) {
                throw new IllegalArgumentException("Key: \"" + key + "\" too long");
            }
            final Option<String> option = userSettings.getString(key);
            for (String value : option) {
                if (value.length() > UserSettingsService.MAX_STRING_VALUE_LENGTH) {
                    throw new IllegalArgumentException("Value: \"" + value + "\" too long");
                }
            }
        }

        // This is not performant, but no product should be basing their impl on the Refapp's
        SettingsMap settingsMap = new SettingsMap(username);
        settingsMap.clear();

        for (String key : userSettings.getKeys()) {
            // can short circuit in each for loop, as options are either of size 0 or 1 (and we know that if one of the
            // options is non-empty, all others will be empty
            for (Boolean value : userSettings.getBoolean(key)) {
                settingsMap.put(key, "B" + value);
            }
            for (Long value : userSettings.getLong(key)) {
                settingsMap.put(key, "L" + value);
            }
            for (String value : userSettings.getString(key)) {
                settingsMap.put(key, "S" + value);
            }
        }
    }

    @Override
    public void updateUserSettings(UserKey userKey, Function<UserSettingsBuilder, UserSettings> updateFunction) {
        checkNotNull(userKey, "userKey cannot be null");
        updateUserSettings(userKey.getStringValue(), updateFunction);
    }

    // workaround for SAL-343
    private static class UserSettingsBuilderRefAppImpl implements UserSettingsBuilder {
        static UserSettingsBuilderRefAppImpl builder() {
            return new UserSettingsBuilderRefAppImpl(DefaultUserSettings.builder());
        }

        static UserSettingsBuilderRefAppImpl builder(UserSettings settings) {
            return new UserSettingsBuilderRefAppImpl(DefaultUserSettings.builder(settings));
        }

        private final UserSettingsBuilder original;

        private UserSettingsBuilderRefAppImpl(UserSettingsBuilder original) {
            this.original = original;
        }

        @Override
        public UserSettingsBuilder put(String key, String value) {
            return original.put(key, value);
        }

        @Override
        public UserSettingsBuilder put(String key, boolean value) {
            return original.put(key, value);
        }

        @Override
        public UserSettingsBuilder put(String key, long value) {
            return original.put(key, value);
        }

        @Override
        public UserSettingsBuilder remove(String key) {
            return original.remove(key);
        }

        @Override
        public Option<Object> get(String key) {
            return original.get(key);
        }

        @Override
        public Set<String> getKeys() {
            // make defensive copy to allow for iterating over the keys by clients
            return ImmutableSet.copyOf(original.getKeys());
        }

        @Override
        public UserSettings build() {
            // double copy to work around SAL-343 - this will prevent manipulating the settings by manipulating this
            // builder
            return DefaultUserSettings.builder(original.build()).build();
        }
    }
}
