package com.atlassian.refapp.sal.search;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.search.SearchMatch;
import com.atlassian.sal.api.search.SearchProvider;
import com.atlassian.sal.api.search.SearchResults;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.core.search.BasicResourceType;
import com.atlassian.sal.core.search.BasicSearchMatch;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@ExportAsService
@Named("RefimplSearchProvider")
public class RefimplSearchProvider implements SearchProvider {
    private static final Logger log = Logger.getLogger(RefimplSearchProvider.class);
    private ApplicationProperties applicationProperties;

    @Inject
    public RefimplSearchProvider(ApplicationProperties props) {
        this.applicationProperties = props;
    }

    public SearchResults search(String username, String searchString) {
        final List<SearchMatch> matches = new ArrayList<SearchMatch>();
        matches.add(new BasicSearchMatch("http://foo.com", "My Foo", "This is the excerpt", new BasicResourceType(applicationProperties, "someType")));
        matches.add(new BasicSearchMatch("http://bar.com", "My Bar", "This is the bar excerpt", new BasicResourceType(applicationProperties, "someType2")));
        return new SearchResults(matches, 2, 666);
    }

    @Override
    public SearchResults search(UserKey userKey, String searchString) {
        if (userKey != null) {
            return search(userKey.getStringValue(), searchString);
        }
        return null;
    }
}
