package com.atlassian.refapp.sal.user;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.user.User;

import java.net.URI;
import java.util.TimeZone;

import static com.google.common.base.Preconditions.checkNotNull;

public class RefimplUserProfile implements UserProfile {
    private static final URI LARGE_CHARLIE = URI.create("/charlie_lg.png");

    private final String username;
    private final String fullName;
    private final String email;
    private final URI profilePageUri;

    public RefimplUserProfile(User user) {
        this.username = checkNotNull(user.getName(), "username");
        this.fullName = user.getFullName();
        this.email = user.getEmail();
        this.profilePageUri = URI.create("/plugins/servlet/users/" + username);
    }

    @Override
    public UserKey getUserKey() {
        // No separate primary key, so return the username
        return new UserKey(username);
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public URI getProfilePictureUri(int width, int height) {
        if (width > 100 || height > 100) {
            return null;
        } else {
            return LARGE_CHARLIE;
        }
    }

    @Override
    public URI getProfilePictureUri() {
        return LARGE_CHARLIE;
    }

    @Override
    public URI getProfilePageUri() {
        return profilePageUri;
    }

    // TODO: Remove this method when SAL-162 has been finished
    public TimeZone getTimeZone() {
        return null;
    }
}
