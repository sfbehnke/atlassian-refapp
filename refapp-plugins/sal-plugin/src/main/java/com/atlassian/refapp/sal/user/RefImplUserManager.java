package com.atlassian.refapp.sal.user;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.user.UserResolutionException;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.user.EntityException;
import com.atlassian.user.Group;
import com.atlassian.user.GroupManager;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;
import com.atlassian.user.search.page.Pager;
import com.atlassian.user.security.authentication.Authenticator;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.startsWith;

/**
 * Pretends the 'someUser' is logged in and is an admin
 */
@ExportAsService
@Named("salUserManager")
public class RefImplUserManager implements com.atlassian.sal.api.user.UserManager {
    private final Logger log = Logger.getLogger(getClass());

    private final AuthenticationContext authenticationContext;
    private final GroupManager groupManager;
    private final UserManager userManager;
    private final Authenticator authenticator;

    @Inject
    public RefImplUserManager(final AuthenticationContext authenticationContext, final UserManager userManager,
                              final GroupManager groupManager, final Authenticator authenticator) {
        this.authenticationContext = assertNotNull(authenticationContext, "authenticationContext");
        this.userManager = assertNotNull(userManager, "userManager");
        this.groupManager = assertNotNull(groupManager, "groupManager");
        this.authenticator = assertNotNull(authenticator, "authenticator");
    }

    public String getRemoteUsername() {
        final Principal user = authenticationContext.getUser();
        if (user == null)
            return null;
        return user.getName();
    }

    @Override
    public UserProfile getRemoteUser() {
        final Principal reqUser = authenticationContext.getUser();
        if (reqUser == null)
            return null;

        return getUserProfile(reqUser.getName());
    }

    @Nullable
    @Override
    public UserKey getRemoteUserKey() {
        final Principal reqUser = authenticationContext.getUser();
        if (reqUser == null)
            return null;

        return new UserKey(reqUser.getName());
    }

    public String getRemoteUsername(final HttpServletRequest request) {
        return request.getRemoteUser();
    }

    @Override
    public UserProfile getRemoteUser(HttpServletRequest request) {
        return getUserProfile(request.getRemoteUser());
    }

    @Nullable
    @Override
    public UserKey getRemoteUserKey(HttpServletRequest request) {
        return new UserKey(request.getRemoteUser());
    }

    public UserProfile getUserProfile(String username) {
        final User user;
        try {
            user = userManager.getUser(username);
            if (user == null) {
                return null;
            }
            return new RefimplUserProfile(user);
        } catch (EntityException e) {
            return null;
        }
    }

    @Nullable
    @Override
    public UserProfile getUserProfile(@Nullable UserKey userKey) {
        if (userKey == null) {
            return null;
        }
        return getUserProfile(userKey.getStringValue());
    }

    public boolean isSystemAdmin(final String username) {
        return isUserInGroup(username, "system_administrators");
    }

    @Override
    public boolean isSystemAdmin(UserKey userKey) {
        if (userKey != null) {
            return isSystemAdmin(userKey.getStringValue());
        }
        return false;
    }

    public boolean isAdmin(final String username) {
        return isSystemAdmin(username) || isUserInGroup(username, "administrators");
    }

    @Override
    public boolean isAdmin(UserKey userKey) {
        if (userKey != null) {
            return isAdmin(userKey.getStringValue());
        }
        return false;
    }

    public boolean authenticate(final String username, final String password) {
        try {
            final boolean authenticated = authenticator.authenticate(username, password);
            if (!authenticated) {
                log.info("Cannot authenticate user '" + username + "' as they used an incorrect password");
            }
            return authenticated;
        } catch (final EntityException e) {
            log.info("Cannot authenticate user '" + username + "' as they do not exist.");
            return false;
        }
    }

    public boolean isUserInGroup(final String username, final String group) {
        try {
            final User user = userManager.getUser(username);
            final Group adminGroup = groupManager.getGroup(group);
            if ((user == null) || (adminGroup == null)) {
                return false;
            }
            return groupManager.hasMembership(adminGroup, user);
        } catch (final EntityException e) {
            return false;
        }
    }

    @Override
    public boolean isUserInGroup(UserKey userKey, String groupName) {
        if (userKey != null) {
            return isUserInGroup(userKey.getStringValue(), groupName);
        }
        return false;
    }

    public Principal resolve(final String username) throws UserResolutionException {
        try {
            if (userManager.getUser(username) == null) {
                return null;
            }
        } catch (final EntityException e) {
            throw new UserResolutionException("Exception resolving user  '" + username + "'.", e);
        }
        return new Principal() {
            public String getName() {
                return username;
            }
        };
    }

    @Override
    public Iterable<String> findGroupNamesByPrefix(String prefix, int startIndex, int maxResults) {
        List<String> groupNames = Lists.newArrayList();
        try {
            Pager allGroups = groupManager.getGroups();

            allGroups.skipTo(startIndex);

            for (Iterator iterator = allGroups.iterator(); iterator.hasNext(); ) {
                if (groupNames.size() >= maxResults) {
                    break;
                }

                Group group = (Group) iterator.next();
                if ((isBlank(prefix) || startsWith(group.getName(), prefix))) {
                    groupNames.add(group.getName());
                }
            }
        } catch (EntityException e) {
            return Collections.emptyList();
        }

        return groupNames;
    }

    /**
     * Check that {@code reference} is not {@code null}. If it is, throw a
     * {@code IllegalArgumentException}.
     *
     * @param reference    reference to check is {@code null} or not
     * @param errorMessage com.atlassian.refapp.sal.message passed to the {@code IllegalArgumentException} constructor
     *                     to give more context when debugging
     * @return {@code reference} so it may be used
     * @throws IllegalArgumentException if {@code reference} is {@code null}
     */
    private static <T> T assertNotNull(final T reference, final Object errorMessage) {
        if (reference == null) {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
        return reference;
    }
}
