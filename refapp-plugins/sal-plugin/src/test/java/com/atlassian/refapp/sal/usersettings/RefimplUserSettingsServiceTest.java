package com.atlassian.refapp.sal.usersettings;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.google.common.base.Function;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.security.Principal;
import java.util.concurrent.atomic.AtomicReference;

import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RefimplUserSettingsServiceTest {
    public static final UserKey TEST_USER = new UserKey("test");
    @Mock
    private UserManager userManager;

    private RefimplUserSettingsService userSettingsService;

    @Before
    public void createService() {
        userSettingsService = new RefimplUserSettingsService(userManager);
    }

    @Before
    public void setUpUser() {
        Principal mockPrincipal = mock(Principal.class);
        when(userManager.resolve(TEST_USER.getStringValue())).thenReturn(mockPrincipal);
    }

    @Test
    public void shouldAllowToAddMultipleSettings() {
        userSettingsService.updateUserSettings(TEST_USER, new Function<UserSettingsBuilder, UserSettings>() {
            @Override
            public UserSettings apply(UserSettingsBuilder builder) {
                return builder.put("test-boolean1", true)
                        .put("test-boolean2", false)
                        .put("test-string1", "string1")
                        .put("test-string2", "string2")
                        .put("test-long1", 1L)
                        .put("test-long2", 2L)
                        .build();
            }
        });

        UserSettings settings = userSettingsService.getUserSettings(TEST_USER);
        assertThat(settings.getKeys(), Matchers.<String>iterableWithSize(6));
        assertBooleanOption(settings, "test-boolean1", true);
        assertBooleanOption(settings, "test-boolean2", false);
        assertStringOption(settings, "test-string1", "string1");
        assertStringOption(settings, "test-string2", "string2");
        assertLongOption(settings, "test-long1", 1L);
        assertLongOption(settings, "test-long2", 2L);
    }

    @Test
    public void shouldAllowToRemoveSettings() {
        userSettingsService.updateUserSettings(TEST_USER, new Function<UserSettingsBuilder, UserSettings>() {
            @Override
            public UserSettings apply(UserSettingsBuilder builder) {
                return builder.put("test-boolean", true)
                        .put("test-string", "string")
                        .put("test-long", 1L)
                        .build();
            }
        });

        UserSettings settings = userSettingsService.getUserSettings(TEST_USER);
        assertThat(settings.getKeys(), Matchers.<String>iterableWithSize(3));

        // now remove all keys
        userSettingsService.updateUserSettings(TEST_USER, new Function<UserSettingsBuilder, UserSettings>() {
            @Override
            public UserSettings apply(UserSettingsBuilder builder) {
                for (String key : builder.getKeys()) {
                    builder.remove(key);
                }
                return builder.build();
            }
        });

        assertThat(userSettingsService.getUserSettings(TEST_USER).getKeys(), emptyIterable());
    }

    @Test
    public void shouldNotAllowManipulatingSettingsByManipulatingTheOriginalBuilder() {
        final AtomicReference<UserSettingsBuilder> builderRef = new AtomicReference<UserSettingsBuilder>();
        userSettingsService.updateUserSettings(TEST_USER, new Function<UserSettingsBuilder, UserSettings>() {
            @Override
            public UserSettings apply(UserSettingsBuilder builder) {
                builderRef.set(builder);
                return builder.put("test-boolean", true).build();
            }
        });

        // add extra key to the builder
        builderRef.get().put("extra-key", false);

        // settings should still have 1 key only
        UserSettings settings = userSettingsService.getUserSettings(TEST_USER);
        assertThat(settings.getKeys(), Matchers.<String>iterableWithSize(1));
    }

    private static void assertBooleanOption(UserSettings settings, String key, boolean value) {
        assertTrue(settings.getBoolean(key).isDefined());
        assertEquals(value, settings.getBoolean(key).get());
    }

    private static void assertStringOption(UserSettings settings, String key, String value) {
        assertTrue(settings.getString(key).isDefined());
        assertEquals(value, settings.getString(key).get());
    }

    private static void assertLongOption(UserSettings settings, String key, long value) {
        assertTrue(settings.getLong(key).isDefined());
        assertEquals(value, (long) settings.getLong(key).get());
    }
}
