package com.atlassian.refapp.webitem;

import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.DefaultWebSectionModuleDescriptor;

public class RefAppWebSectionModuleDescriptor extends DefaultWebSectionModuleDescriptor {
    public RefAppWebSectionModuleDescriptor(WebInterfaceManager webInterfaceManager) {
        super(webInterfaceManager);
    }
}
