package com.atlassian.webdriver.refapp.component;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.component.Header;
import com.atlassian.pageobjects.component.WebSudoBanner;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.utils.Check;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;
import java.util.List;
import java.util.Locale;

/**
 *
 */
public class RefappHeader implements Header {
    private static final By LOGIN = By.id("login");

    @Inject
    protected AtlassianWebDriver driver;

    @Inject
    protected PageBinder pageBinder;

    public boolean isAtActivityStreamsPage() {
        final List<WebElement> activityStreamsLinks = driver.findElements(By.id("activityStreamsLink"));

        if (activityStreamsLinks.isEmpty() || activityStreamsLinks.size() > 1) {
            return false;
        }

        return "Activity Streams".equals(Iterables.getOnlyElement(activityStreamsLinks).getText());
    }

    public boolean isLoggedIn() {
        return driver.findElement(LOGIN).getText().equals("Logout");
    }

    public boolean isAdmin() {
        return loggedInFullNameContains("admin");
    }

    public boolean isSysadmin() {
        return loggedInFullNameContains("sysadmin");
    }

    private boolean loggedInFullNameContains(String text) {
        if (isLoggedIn()) {
            final String userLowerCase = driver.findElement(By.id("user")).getText().toLowerCase(Locale.ENGLISH);
            return userLowerCase.contains(text);
        }
        return false;
    }

    public <M extends Page> M logout(Class<M> nextPage) {
        if (isLoggedIn()) {
            driver.findElement(LOGIN).click();
        }
        return HomePage.class.isAssignableFrom(nextPage) ? pageBinder.bind(nextPage) : pageBinder.navigateToAndBind(nextPage);
    }

    @Override
    public WebSudoBanner getWebSudoBanner() {
        // TODO Auto-generated method stub
        return new WebSudoBanner() {

            WebElement banner() {
                return driver.findElement(By.id("websudo-banner"));
            }

            @Override
            public boolean isShowing() {
                try {
                    return banner().isDisplayed();
                } catch (NoSuchElementException nsee) {
                    return false;
                }
            }

            @Override
            public String getMessage() {
                try {
                    return banner().getText();
                } catch (NoSuchElementException nsee) {
                    return null;
                }
            }

            /**
             * Drops the websudo privilege if the websudo banner is displayed otherwise just navigates
             * to the next page.
             * @param nextPage the page to navigate to after websudo privileges have been dropped.
             * @return The nextPage pageObject
             */
            @Override
            public <P extends Page> P dropWebSudo(Class<P> nextPage) {
                final By webSudoDropButton = By.id("websudo-drop");
                if (Check.elementExists(webSudoDropButton, driver)) {
                    driver.findElement(webSudoDropButton).click();
                    return HomePage.class.isAssignableFrom(nextPage) ? pageBinder.bind(nextPage) : pageBinder.navigateToAndBind(nextPage);
                } else {
                    return pageBinder.navigateToAndBind(nextPage);
                }
            }
        };
    }
}
